package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;

import actors.TilemapActor;
import controllers.DialogueController;


public abstract class BaseScreen implements Screen, InputProcessor
{
    protected Stage mainStage;	// Stage for main textures/objects
    protected Stage uiStage;	// Stage for user interface textures/objects
    protected Table uiTable;	// Used to layout UI widgets
    protected DialogueController dialogueController;
    private InputMultiplexer im = (InputMultiplexer)Gdx.input.getInputProcessor();
    
    public BaseScreen() {
    	//Instantiates stages
        mainStage = new Stage( new FitViewport(TilemapActor.windowWidth,TilemapActor.windowHeight) ); 
        uiStage = new Stage(new FitViewport(TilemapActor.windowWidth,TilemapActor.windowHeight));
 
        // Table created for laying out UI objects
        uiTable = new Table();
        uiTable.setFillParent(true);
        uiStage.addActor(uiTable);

        initialize();
    }

    public abstract void initialize();

    public abstract void update(float dt);

    
    public void render(float dt) {
    	// Limit time that can pass while window is dragged.
    	dt = Math.min(dt, 1/30f);
        
        uiStage.act(dt);
        mainStage.act(dt);
        
        update(dt);
        
        // Clear screen.
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        // Draw screen.
        mainStage.draw(); // Rendered first on screen
        uiStage.draw();   // Rendered on top of mainStage (user interface)
    }

    // Methods required by Screen interface.
    public void resize(int width, int height) {  }

    public void pause()   {  }

    public void resume()  {  }

    public void dispose() {  }

    public void show() {  
    	/* Adds stage objects and BaseScreen to the input multiplexer. */
        im.addProcessor(this);
        im.addProcessor(uiStage);
        im.addProcessor(mainStage);
       
    }
   
    public void hide() {
    	/* Removes the objects from the multiplexer when the screen is not shown. */
        im.removeProcessor(this);
        im.removeProcessor(uiStage);
        im.removeProcessor(mainStage);
        im.removeProcessor(dialogueController);
    }
    
    public void addDialogueController(DialogueController dialogueController) {
    	im.addProcessor(dialogueController);
    } 
    
    // methods required by InputProcessor interface
    public boolean keyDown(int keycode)
    {  return false;  }

    public boolean keyUp(int keycode)
    {  return false;  }

    public boolean keyTyped(char c) 
    {  return false;  }

    public boolean mouseMoved(int screenX, int screenY)
    {  return false;  }

    public boolean scrolled(int amount) 
    {  return false;  }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) 
    {  return false;  }

    public boolean touchDragged(int screenX, int screenY, int pointer) 
    {  return false;  }

    public boolean touchUp(int screenX, int screenY, int pointer, int button) 
    {  return false;  }
}

    
    