package screens;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import actors.BaseActor;
import actors.Dean;
import actors.DialogueBox;
import actors.Enemy;
import actors.PlayableCharacter;
import actors.Rope;
import actors.Solid;
import actors.StationaryCharacter;
import actors.Teleporter;
import actors.TilemapActor;
import controllers.DialogueController;
import controllers.MusicController;
import controllers.PartyController;
import dialogue.Dialogue;
import dialogue.DialogueNode;
import game.BaseGame;
import game.FreshersEscapade;

public class GameScreen extends BaseScreen{
	
	private PartyController partyController;
	private TilemapActor tma;
	private DialogueBox dialogueBox;
	private Dialogue introDialogue, lucyDialogue, hankDialogue, winDialogue, loseDialogue, yenneferDialogue, bossDialogue;
	private DialogueController dialogueController;
	public static enum gameState {RUNNING, PAUSED};
	private gameState state;
	private MusicController musicController;
	private PlayableCharacter activePC, lucyPC, hankPC, yenneferPC; 
	private List<PlayableCharacter> generatedPCList;
	private StationaryCharacter lucy, hank, yennefer, enemy;

	
	@Override
	public void initialize() {

		state = gameState.RUNNING; // Initialise game state to running.
		initDialogue();
		musicController = new MusicController();
		partyController = BaseGame.partyController;
		generatedPCList = new ArrayList<PlayableCharacter>(4);
		
		
		switch(BaseGame.activeScreen) {
			case ("game") : {
					tma = new TilemapActor("gameScreen/map.tmx", mainStage);
					if(BaseGame.openingFinished == false) {
						dialogueController.startDialogue(introDialogue);
						BaseGame.openingFinished = true;
				}
			} break;
			case ("club1") : {
				musicController.setMusic("club1");
				tma = new TilemapActor("gameScreen/club1.tmx", mainStage);
			} break;
			case ("club2") : {
				musicController.setMusic("club2");
				tma = new TilemapActor("gameScreen/club2.tmx", mainStage);
			} break;
			case ("club3") :  {
				musicController.setMusic("club3");
				tma = new TilemapActor("gameScreen/club3.tmx", mainStage);
			} break;
			case ("club4") :  {
				musicController.setMusic("club4");
				tma = new TilemapActor("gameScreen/club4.tmx", mainStage);
			} break;
			case ("club5") :  {
				musicController.setMusic("club5");
				tma = new TilemapActor("gameScreen/club5.tmx", mainStage);
			} break;
			case("club6") : {
				musicController.setMusic("club6");
				tma = new TilemapActor("gameScreen/club6.tmx", mainStage);
			}
		}
		
		musicController.play();

		/* Initialises solid objects */
        for (MapObject obj : tma.getRectangleList("Solid") ) {
            MapProperties props = obj.getProperties();
            new Solid( (float)props.get("x"), (float)props.get("y"), (float)props.get("width"), (float)props.get("height"), mainStage );
        }
        
        /* Initialises enemy objects and sets their health using their tiled map properties. */
        for (MapObject obj : tma.getTileList("Enemy")) {
        	MapProperties props = obj.getProperties();
        	Enemy e = new Enemy( (float)props.get("x"), (float)props.get("y"), mainStage );
        	e.setE1Hp( (int)props.get("hp1") );
        	e.setE2Hp( (int)props.get("hp2") );
        	e.setE3Hp( (int)props.get("hp3") );
        	e.setID( (int)props.get("id") );
        	e.setMinDmg( (int)props.get("minDmg") );
        	e.setMaxDmg( (int)props.get("maxDmg") );
        }

        for (MapObject obj : tma.getTileList("Lucy")) {
        	MapProperties props = obj.getProperties(); 
        	 if (props.get("name").equals("Lucy")) {
        		//lucyPC = new PlayableCharacter( (float)props.get("x"), (float)props.get("y"), mainStage, 20, 2, 4, 5, "gameScreen/Lucy.png", "Lucy");
        		lucy = new StationaryCharacter((float)props.get("x"), (float)props.get("y"), mainStage, "gameScreen/Lucy.png", lucyDialogue, lucyPC);
        		
        	 } else if (props.get("name").equals("Yennefer")) {
        		yennefer = new StationaryCharacter((float)props.get("x"), (float)props.get("y"), mainStage, "gameScreen/yennefer.png", yenneferDialogue, yenneferPC);
        		//yenneferPC = new PlayableCharacter( (float)props.get("x"), (float)props.get("y"), mainStage, 25, 3, 5, 0, "gameScreen/yennefer.png", "Yennefer");
        	} else  if(props.get("name").equals("Hank")) {
        		hank = new StationaryCharacter((float)props.get("x"), (float)props.get("y"), mainStage, "gameScreen/Hank.png", hankDialogue, hankPC);
            	//hankPC = new PlayableCharacter( (float)props.get("x"), (float)props.get("y"), mainStage, 50, 4, 2, 0, "gameScreen/Hank.png", "Hank");
        	} else if (props.get("name").equals("enemy")) {
                enemy = new StationaryCharacter( (float)props.get("x"), (float)props.get("y"), mainStage, "gameScreen/enemy.png", bossDialogue, null );
            }
        }
        
        hankPC = new PlayableCharacter(0, 0, mainStage, 50, 4, 2, 0, "gameScreen/Hank.png", "Hank");

        yenneferPC = new PlayableCharacter(0, 0, mainStage, 25, 3, 5, 0, "gameScreen/yennefer.png", "Yennefer");

        lucyPC = new PlayableCharacter( 0,0, mainStage, 20, 2, 4, 5, "gameScreen/Lucy.png", "Lucy");
        partyController.addMember(hankPC);
        partyController.addMember(lucyPC);
        partyController.addMember(yenneferPC);
        
        for (MapObject obj : tma.getTileList("Dean")) {
        	MapProperties props = obj.getProperties();
        	new StationaryCharacter( (float)props.get("x"), (float)props.get("y"), mainStage, "gameScreen/Dean.png", null, null );
        }
        
        /* Initialises teleporter and sets ID based on tile map properties */
        for (MapObject obj : tma.getRectangleList("Teleporter") ) {
            MapProperties props = obj.getProperties();
            Teleporter t = new Teleporter( (float)props.get("x"), (float)props.get("y"), (float)props.get("width"), (float)props.get("height"), mainStage );
            t.setID(props.get("id").toString());
        }
        
        /* Initialises rope and sets ID based on tile map properties */
        for (MapObject obj : tma.getTileList("Rope") ) {
            MapProperties props = obj.getProperties();
            Rope r = new Rope( (float)props.get("x"), (float)props.get("y"), (float)props.get("width"), (float)props.get("height"), mainStage );
            r.setID( (String)props.get("id") );
        }
               
        /* Creates player at the map's defined starting point, at the rectangular object with property "Start". */
        MapObject startPoint = tma.getRectangleList("Start").get(0);
        MapProperties startProps = startPoint.getProperties();
        activePC = new PlayableCharacter( (float)startProps.get("x"), (float)startProps.get("y"), mainStage, 30, 7, 3, 0, "gameScreen/player.png", "Dave");
        
        
    	BaseGame.partyController.addMember(activePC);
    	
        /* Creates a text button with the text Main Menu using the styles defined in the base game class */
		TextButton menuButton = new TextButton( "Main Menu", BaseGame.textButtonStyle );
		
		// When the text button is clicked, the active screen is set to the main menu screen.
		menuButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(!BaseGame.activeScreen.equals("game"))
					musicController.dispose();
				BaseGame.activeScreen = "menu";
				FreshersEscapade.setActiveScreen(new MenuScreen());
			}
		});
        
		TextButton saveButton = new TextButton("Save", BaseGame.textButtonStyle);
		
		saveButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				try {
					ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("SavedContent.txt"));
					System.out.println("Saved");
					System.out.println(BaseGame.partyController.getPC(0).getHp());
					System.out.println(BaseGame.partyController.getPC(0).getMana());
					

					
					out.writeObject(partyController.getPC(0).getHp());
					//out.writeObject(player.getp1Mana());
				} catch (FileNotFoundException e) {
					File newFile = new File("SavedContent.txt");
					try {
						newFile.createNewFile();
						ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("SavedContent.txt"));
						//out.writeObject();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
						e.printStackTrace();
				} catch (IOException e) {
					File newFile = new File("SavedContent.txt");
					try {
						newFile.createNewFile();
						ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("SavedContent.txt"));
						//out.writeObject();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					e.printStackTrace();
				}
			}
		});
		
        /* Creates a text button with the text Quit using the styles defined in the base game class */
		TextButton quitButton = new TextButton( "Quit", BaseGame.textButtonStyle );
		
		// When the text button is clicked, the application is closed.
		quitButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});
		
		uiTable.align (Align.center|Align.top);
		uiTable.padTop(50);
		uiTable.add(menuButton).padBottom(50);
		uiTable.row();
		uiTable.add(saveButton).padBottom(50);
		uiTable.row();
		uiTable.add(quitButton);
		
		uiTable.setVisible(false);
		
       }
			
	@Override
	public void update(float dt) {
		
		switch (state) {
			case RUNNING: {
					if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
						activePC.setAnimationPaused(true);
						activePC.setSpeed(0);
						try {
							Thread.sleep(200);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						dialogueBox.setVisible(false);
						uiTable.setVisible(true);
						if(!BaseGame.activeScreen.equals("game"))
							musicController.pause();
						state = gameState.PAUSED;
					}
	
				// Player movement controls.
				if (Gdx.input.isKeyPressed(Keys.LEFT))
					activePC.accelerateAtAngle(180);
				if (Gdx.input.isKeyPressed(Keys.RIGHT))
					activePC.accelerateAtAngle(0);
				if (Gdx.input.isKeyPressed(Keys.UP))
					activePC.accelerateAtAngle(90);
				if (Gdx.input.isKeyPressed(Keys.DOWN))
					activePC.accelerateAtAngle(270);
				
				// Prevents player from travelling through solid objects.
				for (BaseActor solid : BaseActor.getList(mainStage, "actors.Solid")) {
				 activePC.preventOverlap(solid);
				}
				
				if (BaseGame.activeScreen == "club2") {
					if (BaseActor.count(mainStage, "actors.Enemy") == 0) {
						musicController.dispose();
						BaseGame.activeScreen = "game";
						FreshersEscapade.setActiveScreen(new GameScreen());
						BaseGame.club2Complete = true;
					}
				}
				
				if (BaseGame.activeScreen == "club3") {
					if (BaseActor.count(mainStage, "actors.Enemy") == 0) {
						musicController.dispose();
						BaseGame.activeScreen = "game";
						FreshersEscapade.setActiveScreen(new GameScreen());
						BaseGame.club3Complete = true;
					}
				}
				
				if (BaseGame.activeScreen == "club4") {
					if (BaseActor.count(mainStage, "actors.Enemy") == 0) {
						musicController.dispose();
						BaseGame.activeScreen = "game";
						FreshersEscapade.setActiveScreen(new GameScreen());
						BaseGame.club4Complete = true;
					}
				}
				
				if (BaseGame.activeScreen == "club5") {
					if (BaseActor.count(mainStage, "actors.Enemy") == 0) {
						musicController.dispose();
						BaseGame.activeScreen = "game";
						FreshersEscapade.setActiveScreen(new GameScreen());
						BaseGame.club5Complete = true;
					}
				}
				
				if (BaseGame.activeScreen == "club6") {
					if (BaseActor.count(mainStage, "actors.Enemy") == 0) {
						musicController.dispose();
						BaseGame.activeScreen = "game";
						FreshersEscapade.setActiveScreen(new GameScreen());
						BaseGame.club5Complete = true;
					}
				}
				
				// Prevent player from overlapping rope, when all enemies are defeated; remove rope 
				for (BaseActor rope : BaseActor.getList(mainStage, "actors.Rope")) {
					 activePC.preventOverlap(rope);
//					 if (BaseActor.count(mainStage, "actors.Enemy") == 0) {
						 rope.remove();
//					}
				}
				
				for (BaseActor dean : BaseActor.getList(mainStage, "actors.Dean")) {
					 activePC.preventOverlap(dean);
				}
				
				// Teleports player to new map if player collides with teleporter, when active screen screen is club1; the game screen loads the club1 map. Also stops player overlapping teleporters
				for (BaseActor teleporter : BaseActor.getList(mainStage, "actors.Teleporter")) {
					Teleporter tp = (Teleporter)teleporter;
					activePC.preventOverlap(teleporter);
					 
					if (activePC.overlaps(tp)) {
						if (tp.getID().equals("Club1")) {
							if (BaseGame.club1Complete == false) {
							BaseGame.activeScreen = "club1";
							FreshersEscapade.setActiveScreen(new GameScreen());
							musicController.setMusic("club1");
							}
						}
						
						if (tp.getID().equals("Club2")) {
							if (BaseGame.club1Complete == true && BaseGame.club2Complete == false) {
							BaseGame.activeScreen = "club2";
							FreshersEscapade.setActiveScreen(new GameScreen());
							musicController.setMusic("club2");
							}
						}
						
						if (tp.getID().equals("Club3")) {
							if (BaseGame.club2Complete == true && BaseGame.club3Complete == false) {
								BaseGame.activeScreen = "club3";
								FreshersEscapade.setActiveScreen(new GameScreen());
								musicController.setMusic("club3");
							}
						}
						
						if (tp.getID().equals("Club4")) {
							if (BaseGame.club3Complete == true && BaseGame.club4Complete == false) {
								BaseGame.activeScreen = "club4";
								FreshersEscapade.setActiveScreen(new GameScreen());
								musicController.setMusic("club4");
							}
						}
						
						if (tp.getID().equals("Club5")) {
							if (BaseGame.club4Complete == true && BaseGame.club5Complete == false) {
								BaseGame.activeScreen = "club5";
								FreshersEscapade.setActiveScreen(new GameScreen());
								musicController.setMusic("club5");
							}
						}
						
						if (tp.getID().equals("Club6")) {
							if (BaseGame.club5Complete == true) {
								BaseGame.activeScreen = "club6";
								FreshersEscapade.setActiveScreen(new GameScreen());
								musicController.setMusic("club6");
							}
						}
						
						if (tp.getID().equals("Club1Exit") || tp.getID().equals("Club2Exit") || tp.getID().equals("Club3Exit") 
							|| tp.getID().equals("Club4Exit") || tp.getID().equals("Club5Exit")){
							BaseGame.activeScreen = "game";
							FreshersEscapade.setActiveScreen(new GameScreen());
							musicController.dispose();
						}
					}
				}
				
				for (BaseActor sc : BaseActor.getList(mainStage, "actors.StationaryCharacter")) {
					StationaryCharacter tempSC = (StationaryCharacter) sc;
					activePC.preventOverlap(sc);
					 if (activePC.overlaps(sc)) {
						 BaseGame.club1Complete = true;
						 dialogueController.startDialogue(tempSC.getDialogue());
						 
									 
						 if (dialogueController.getFinished() == true) {
							 BaseGame.activeScreen = "game";
							 musicController.dispose();
							 FreshersEscapade.setActiveScreen(new GameScreen());
							 if(!tempSC.getDialogue().equals(bossDialogue)){
								 BaseGame.partyController.addMember(tempSC.getPC());
						 }	 }
					 }
				}
				 	 
					
				// If the player collides (overlaps) with an enemy, switch to the battle screen
				for (BaseActor enemy : BaseActor.getList(mainStage, "actors.Enemy")) {
					 if (activePC.overlaps(enemy)) {
						 FreshersEscapade.setActiveScreen( new BattleScreen(enemy, this) );
					 }
				}
			} break;
			case PAUSED: {
				if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(!BaseGame.activeScreen.equals("game"))
						musicController.play();
					uiTable.setVisible(false);
					dialogueBox.setVisible(true);
					state = gameState.RUNNING;
				}	
			} break;
		}
	}

	public void initDialogue() {
		dialogueBox = new DialogueBox(0, 0, uiStage);
		dialogueBox.setBackgroundColor(Color.WHITE);
		dialogueBox.setFontColor(Color.BLACK);
		dialogueBox.setDialogueSize(600, 100);
		dialogueBox.setFontScale(0.80f);
		dialogueBox.alignCenter();
		dialogueBox.setVisible(false);
		
		dialogueController = new DialogueController(dialogueBox);
		super.addDialogueController(dialogueController);
		
		introDialogue = new Dialogue();
		DialogueNode node1 = new DialogueNode("START: PRESS A", 0);
		DialogueNode node2 = new DialogueNode("???: Heroes I have teleported you here so you can save us from ... wait where are the rest of you?", 1);
		DialogueNode node3 = new DialogueNode("Dave: Where the hell am I? Who are you?", 2);
		DialogueNode node4 = new DialogueNode("???: I summoned you here young hero, to my world, to save my university and my students. I am the university's dean.", 3);
		DialogueNode node5 = new DialogueNode("Dave: What, I was just out with my friends, how did I end up here?", 4);
		DialogueNode node6 = new DialogueNode("Dean: Ah I see you do not have magic in your world, this happens quite often. In our world everyone is born with small amounts of magic, whenever a catastrophe befalls, we have to summon heroes from far away worlds to save us.", 5); 
		DialogueNode node7 = new DialogueNode("Dean: However, something went wrong and of the four heroes only you arrived here, the other three must have been intercepted by the enemy. You must save them first before you can save us.", 6);
		DialogueNode node8 = new DialogueNode("Dave: Heroes? I think you mean my friends, where are they? How do I get back to my world? And what do you mean by enemy?", 7);
		DialogueNode node9 = new DialogueNode("Dean: In our world we have monsters, most of them live in the forest and we humans stay away from them in our cities. Recently however they've begun invading cities, and we've had to evacuate.", 8);
		DialogueNode node10 = new DialogueNode("Dean: Someone intelligent is leading them, every hunter we've hired has not returned and they somehow interfered with my summoning.", 9);
		DialogueNode node11 = new DialogueNode("Dean: They've taken refuge in the clubs along these streets after attacking my students during fresher's week. That's where you'll find your comrades.", 10);
		DialogueNode node12 = new DialogueNode("Dean: I need you to defeat the monsters in these clubs and take down the mastermind behind them. Once you've done this, the conditions of the summoning will be complete. You and your friends will be teleported back.", 11);
		DialogueNode node13 = new DialogueNode("Dave: I can't defeat monsters, I've never fought anyone.", 12);
		DialogueNode node14 = new DialogueNode("Dean: In your world you had no magic, here you are much stronger than the humans or monsters in this world. You and your friends should have magic matching your specialities from your world.", 13);
		DialogueNode node15 = new DialogueNode("Dean: You will need them to defeat the stronger monsters that are in control of the clubs. They'll have the keys to the other clubs.", 14);
		DialogueNode node16 = new DialogueNode("Dave: Looks like I have no other choice, I have to save my friends and get out of here.", 15);
		DialogueNode node17 = new DialogueNode("TUTORIAL: Use the arrows keys to move, battle begins on collision with an enemy.", 16);
		DialogueNode node18 = new DialogueNode("TUTORIAL: Players can make one move per turn: " + "\nAttack to do damage" + "\nUse a player-specific skill (costs mana)" + "\nBlock to take half damage from enemy"  , 17);
		DialogueNode node19 = new DialogueNode("TUTORIAL: Enemies attack a random target and do varying amounts of damage"  , 18);

		node1.setNext(node2.getID());
		node2.setNext(node3.getID());
		node3.setNext(node4.getID());
		node4.setNext(node5.getID());
		node5.setNext(node6.getID());
		node6.setNext(node7.getID());
		node7.setNext(node8.getID());
		node8.setNext(node9.getID());
		node9.setNext(node10.getID());
		node10.setNext(node11.getID());
		node11.setNext(node12.getID());
		node12.setNext(node13.getID());
		node13.setNext(node14.getID());
		node14.setNext(node15.getID());
		node15.setNext(node16.getID());
		node16.setNext(node17.getID());
		node17.setNext(node18.getID());
		node18.setNext(node19.getID());
		
		introDialogue.addNode(node1);
		introDialogue.addNode(node2);
		introDialogue.addNode(node3);
		introDialogue.addNode(node4);
		introDialogue.addNode(node5);
		introDialogue.addNode(node6);
		introDialogue.addNode(node7);
		introDialogue.addNode(node8);
		introDialogue.addNode(node9);
		introDialogue.addNode(node10);
		introDialogue.addNode(node11);
		introDialogue.addNode(node12);
		introDialogue.addNode(node13);
		introDialogue.addNode(node14);
		introDialogue.addNode(node15);
		introDialogue.addNode(node16);
		introDialogue.addNode(node17);
		introDialogue.addNode(node18);
		introDialogue.addNode(node19);
		
		
		lucyDialogue = new Dialogue();
		DialogueNode lucyNode1 = new DialogueNode("Dave: Lucy, are you ok?",0);
		DialogueNode lucyNode2 = new DialogueNode("Lucy: Yeah, where the hell are we? I thought I was having a nightmare, I was trapped by those... things.",1);
		DialogueNode lucyNode3 = new DialogueNode("Dave: Apparently, we were teleported to this world by some university dean to save this place from those monsters.",2);
		DialogueNode lucyNode4 = new DialogueNode("Dave: I thought he was crazy at first but I'm actually kind of strong. He said once we've defeated the monsters in the clubs, we'll be able to leave.",3);
		DialogueNode lucyNode5 = new DialogueNode("Dave: The rest of the guys are also trapped here, I was the only one who wasn't, we need to rescue the,. We should all have powers to help us get out of here.",4);
		DialogueNode lucyNode6 = new DialogueNode("Lucy: I healed myself earlier after I got trapped, I think I can heal people.",5);
		DialogueNode lucyNode7 = new DialogueNode("Dave: That will be useful, lets get out of here and look for the rest of the guys.",6);
		DialogueNode lucyNode8 = new DialogueNode("Lucy: Ok, but I don't trust this dean, why would he teleport us here? Let me heal you before we leave, you're hurt.",7);
		DialogueNode lucyNode9 = new DialogueNode("Dave: Oh, thank you.",8);
		DialogueNode lucyNode10 = new DialogueNode("TUTORIAL:  After defeating a club Lucy will heal the party back to full health",9);
		
		lucyNode1.setNext(lucyNode2.getID());
		lucyNode2.setNext(lucyNode3.getID());
		lucyNode3.setNext(lucyNode4.getID());
		lucyNode4.setNext(lucyNode5.getID());
		lucyNode5.setNext(lucyNode6.getID());
		lucyNode6.setNext(lucyNode7.getID());
		lucyNode7.setNext(lucyNode8.getID());
		lucyNode8.setNext(lucyNode9.getID());
		lucyNode9.setNext(lucyNode10.getID());
		
		lucyDialogue.addNode(lucyNode1);
		lucyDialogue.addNode(lucyNode2);
		lucyDialogue.addNode(lucyNode3);
		lucyDialogue.addNode(lucyNode4);
		lucyDialogue.addNode(lucyNode5);
		lucyDialogue.addNode(lucyNode6);
		lucyDialogue.addNode(lucyNode7);
		lucyDialogue.addNode(lucyNode8);
		lucyDialogue.addNode(lucyNode9);
		lucyDialogue.addNode(lucyNode10);
		
		
		hankDialogue= new Dialogue(); 
		DialogueNode hankNode1 = new DialogueNode("Hank: what took you so long, I�ve been waiting for you.",0);
		DialogueNode hankNode2 = new DialogueNode("Dave: Lucy and I had to fight our way to get to you.",1);
		DialogueNode hankNode3 = new DialogueNode("Hank: I�ve been feeling weird lately, I have this unusual surge of energy running through me.",2);
		DialogueNode hankNode4 = new DialogueNode("Yennefer: what do you mean?.",3);
		DialogueNode hankNode5 = new DialogueNode("Hank: I can�t explain this sensation.",4);
		DialogueNode hankNode6 = new DialogueNode("Dave: We need to defeat all the enemies so we can get back home.",5);
		DialogueNode hankNode7 = new DialogueNode("Hank: what are you waiting for lets go!!.",6);
		DialogueNode hankNode8 = new DialogueNode("Lucy: let me heal you first, we need you in your best condition",7);
		DialogueNode hankNode9 = new DialogueNode("Hank: Thank you Lucy.",8);
		DialogueNode hankNode10 = new DialogueNode("TUTORIAL:Hank throw a fireball which deal 6 damage to the enemy.",9);
		
		hankNode1.setNext(hankNode2.getID());
		hankNode2.setNext(hankNode3.getID());
		hankNode3.setNext(hankNode4.getID());
		hankNode4.setNext(hankNode5.getID());
		hankNode5.setNext(hankNode6.getID());
		hankNode6.setNext(hankNode7.getID());
		hankNode7.setNext(hankNode8.getID());
		hankNode8.setNext(hankNode9.getID());
		hankNode9.setNext(hankNode10.getID());
		
		hankDialogue.addNode(hankNode1);
		hankDialogue.addNode(hankNode2);
		hankDialogue.addNode(hankNode3);
		hankDialogue.addNode(hankNode4);
		hankDialogue.addNode(hankNode5);
		hankDialogue.addNode(hankNode6);
		hankDialogue.addNode(hankNode7);
		hankDialogue.addNode(hankNode8);
		hankDialogue.addNode(hankNode9);
		hankDialogue.addNode(hankNode10);
		
		yenneferDialogue= new Dialogue(); 
		DialogueNode yenneferNode1 = new DialogueNode("Yennefer: guys you�re  all safe! I was worried when I got locked here by myself.",0);
		DialogueNode yenneferNode2 = new DialogueNode("Dave: how are you feeling, any weird power running through your veins??.",1);
		DialogueNode yenneferNode3 = new DialogueNode("yennefer: not really but I do have powers, magical powers to be exact .",2);
		DialogueNode yenneferNode4 = new DialogueNode("Lucy: can you heal people like me? ",3);
		DialogueNode yenneferNode5 = new DialogueNode("Yennefer: my power is different I can cast spells, isn�t that cool?.",4);
		DialogueNode yenneferNode6 = new DialogueNode("Dave: that�s cool! Lucy heal mage so that we can go and get out of here and find Hank. ",5);
		DialogueNode yenneferNode7 = new DialogueNode("TUTORIAL: Yennefer can throw a forstball which can deal 5 damage to the enemy.",6);
		
		yenneferNode1.setNext(yenneferNode2.getID());
		yenneferNode2.setNext(yenneferNode3.getID());
		yenneferNode3.setNext(yenneferNode4.getID());
		yenneferNode4.setNext(yenneferNode5.getID());
		yenneferNode5.setNext(yenneferNode6.getID());
		yenneferNode6.setNext(yenneferNode7.getID());
	
		
		yenneferDialogue.addNode(yenneferNode1);
		yenneferDialogue.addNode(yenneferNode2);
		yenneferDialogue.addNode(yenneferNode3);
		yenneferDialogue.addNode(yenneferNode4);
		yenneferDialogue.addNode(yenneferNode5);
		yenneferDialogue.addNode(yenneferNode6);
		yenneferDialogue.addNode(yenneferNode7);
		
		bossDialogue= new Dialogue(); 
		DialogueNode bossNode1 = new DialogueNode("Boss: well, well, well. Welcome to my lair, according to my minions I expected to see you here. I must do everything myself even defeating you humans.",0);
		DialogueNode bossNode2 = new DialogueNode("Boss: I will send you back to your prisons and this time you will never leave EVER!! Buahahahaha.",1);
		DialogueNode bossNode3 = new DialogueNode("Hank: well, that�s a weird laugh�..",2);
		DialogueNode bossNode4 = new DialogueNode("Yennefer: I agree, if you�re trying to sound evil, try this instead Muahahaha!!.",3);
		DialogueNode bossNode5 = new DialogueNode("Hank: oh that�s sounds scarier.",4);
		DialogueNode bossNode6 = new DialogueNode("Lucy: guys stop .",5);
		DialogueNode bossNode7 = new DialogueNode("Boss: : I see, you humans don�t understand fear when faced with it, I shall show you. The only thing between going home and staying here forever is me. Beg me then I might consider it.",6);
		DialogueNode bossNode8 = new DialogueNode("Dave: I would rather fight you! Guys focus we must win!! ",7);
		DialogueNode bossNode9 = new DialogueNode("TUTORIAL:Beat the boss so that you can go home.",8);
		
		
		bossNode1.setNext(bossNode2.getID());
		bossNode2.setNext(bossNode3.getID());
		bossNode3.setNext(bossNode4.getID());
		bossNode4.setNext(bossNode5.getID());
		bossNode5.setNext(bossNode6.getID());
		bossNode6.setNext(bossNode7.getID());
		bossNode7.setNext(bossNode8.getID());
		bossNode8.setNext(bossNode9.getID());
		
		
		bossDialogue.addNode(bossNode1);
		bossDialogue.addNode(bossNode2);
		bossDialogue.addNode(bossNode3);
		bossDialogue.addNode(bossNode4);
		bossDialogue.addNode(bossNode5);
		bossDialogue.addNode(bossNode6);
		bossDialogue.addNode(bossNode7);
		bossDialogue.addNode(bossNode8);
		bossDialogue.addNode(bossNode9);
		
		
		winDialogue= new Dialogue();
		DialogueNode winNode1 = new DialogueNode("Boss: you have strong allies Dave, keep them close.",0);
		DialogueNode winNode2 = new DialogueNode("Dave: One man alone can't defeat the forces of evil, but many good people coming together can -Harold S. Kushner.",2);
		
		winNode1.setNext(winNode2.getID());
		
		winDialogue.addNode(winNode1);
		winDialogue.addNode(winNode2);
		
		loseDialogue= new Dialogue();
		DialogueNode loseNode1 = new DialogueNode("Boss:it�s the end of your journey.  I will always prevail!!.",0);
		DialogueNode loseNode2 = new DialogueNode("Dave: we will never give up!!.",2);
		
		loseNode1.setNext(loseNode2.getID());
		
		loseDialogue.addNode(loseNode1);
		loseDialogue.addNode(loseNode2);
		
		
		
		
	}
	
}
			 
		
		


		
				 
			 
		
	
