package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import actors.BaseActor;
import actors.TilemapActor;
import game.BaseGame;
import game.FreshersEscapade;

public class MenuScreen extends BaseScreen{

	@Override
	public void initialize() {
		/* Initialises the menu screens background image and title (from the assets folder) at the corresponding size. */
		BaseActor menuBackground = new BaseActor(0,0,mainStage);
		menuBackground.loadTexture("menuScreen/greyMenuScreenBackground.png");
		menuBackground.setSize(TilemapActor.windowWidth, TilemapActor.windowHeight);
		
		BaseActor menuTitle = new BaseActor(0,0,mainStage);
		menuTitle.loadTexture("menuScreen/title.png");
		
		/* Creates a text button with the text Start using the styles defined in the base game class */
		TextButton startButton = new TextButton( "Start", BaseGame.textButtonStyle );
		
		// When the text button is clicked, sets the screen to a new game screen
		startButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				BaseGame.activeScreen = "game";
				FreshersEscapade.setActiveScreen( new GameScreen() );
			}
		});
		
		/* Creates a text button with the text Quit using the styles defined in the base game class */
		TextButton quitButton = new TextButton( "Quit", BaseGame.textButtonStyle );
		
		// When the text button is clicked, the application is closed
		quitButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});
		
		// Lays out widgets using scene2d table
		uiTable.align(Align.center|Align.top);
		uiTable.padTop(50);
		uiTable.add(menuTitle).padBottom(50);
		uiTable.row();
		uiTable.add(startButton).padBottom(50);
		uiTable.row();
		uiTable.add(quitButton);
	}
		
	
	@Override
	public void update(float dt) {
	}

}
