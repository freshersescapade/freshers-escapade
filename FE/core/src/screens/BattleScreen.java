package screens;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import actors.BaseActor;
import actors.DialogueBox;
import actors.Enemy;
import actors.Heal;
import actors.HealthBar;
import actors.PlayableCharacter;
import actors.Punch;
import actors.Fireball;
import actors.Frost;
import actors.TilemapActor;
import controllers.PartyController;
import game.BaseGame;
import game.FreshersEscapade;

public class BattleScreen extends BaseScreen {

	private List<PlayableCharacter> battleParty;
	private Enemy enemy;

	private BaseActor BattleScreenActors;

	// Labels
	private Label player1Name;
	private Label player2Name;
	private Label player3Name;
	private Label player4Name;
	private Label player1Mana;
	private Label player2Mana;
	private Label player1Dmg;
	private Label player2Dmg;
	private Label player3Mana;
	private Label player4Mana;
	private Label player3Dmg;
	private Label player4Dmg;
	private Label enemy1Hp;
	private Label enemy2Hp;
	private Label enemy3Hp;

	private Random randomPlayerTarget;
	private Random randomDmg;

	private TextButton player1Button;
	private TextButton player2Button;
	private TextButton player3Button;
	private TextButton player4Button;
	private TextButton enemy1;
	private TextButton enemy2;
	private TextButton enemy3;

	private ButtonStyle attackButtonStyle;
	private ButtonStyle skillButtonStyle;
	private ButtonStyle blockButtonStyle;
	private Button attackButton;
	private Button skillButton;
	private Button blockButton;
	private Button backButton;

	private TextButton executeSkill;
	private TextButton healSkill;
	private TextButton fireSkill;
	private TextButton frostSkill;

	private DialogueBox dialogueBox;

	private GameScreen gameScreen;

	private HealthBar healthBarFore, lucyHealthBarFore, hankHealthBarFore, yenneferHealthBarFore;
	private float stamina = 60;
	private float stamina2 = 20;
	private float stamina3 = 25;
	private float stamina4 = 50;

	private boolean p1But = false, p2But = false, p3But = false, p4But = false;
	private boolean attBut = false, skillBut = false, blockBut = false, execBut = false, backBut = false,
			 frostBut = false, fireBut = false;

	public BattleScreen(BaseActor enemy, GameScreen gameScreen) {
		// Passes actors from gameScreen to battleScreen
		this.enemy = (Enemy) enemy;
		this.gameScreen = gameScreen;
	}

	@Override
	public void initialize() {

		battleParty = BaseGame.partyController.party;

		BaseActor BattleScreenBackground = new BaseActor(0, 0, mainStage);

		switch (BaseGame.activeScreen) {
		case ("club1"): {
			BattleScreenBackground.loadTexture("battleScreen/BattleScreenBackground.png");
		}
			break;
		case ("club2"): {
			BattleScreenBackground.loadTexture("battleScreen/BattleScreenBackground2.png");
		}
			break;
		case ("club3"): {
			BattleScreenBackground.loadTexture("battleScreen/BattleScreenBackground3.png");
		}
			break;
		case ("club4"): {
			BattleScreenBackground.loadTexture("battleScreen/BattleScreenBackground4.png");
		}
			break;
		case ("club5"): {
			BattleScreenBackground.loadTexture("battleScreen/BattleScreenBackground5.png");
		}
			break;
		case ("club6"): {
			BattleScreenBackground.loadTexture("battleScreen/BattleScreenBackground6.png");
		}
			break;
		}

		BattleScreenBackground.setSize(TilemapActor.windowWidth, TilemapActor.windowHeight);
		BattleScreenActors = new BaseActor(0, 0, mainStage);
		BattleScreenActors.setSize(TilemapActor.windowWidth, TilemapActor.windowHeight);

		dialogueBox = new DialogueBox(300, 20, uiStage);
		dialogueBox.setBackgroundColor(Color.GRAY);
		dialogueBox.setFontColor(Color.BLACK);
		dialogueBox.setScale(1f);
		dialogueBox.setDialogueSize(400, 60);
		dialogueBox.setFontScale(0.6f);
		dialogueBox.alignCenter();
		dialogueBox.setVisible(false);

		// Creates label (text) for player and enemy health points and adds them to
		// screen on UI stage (on top of mainStage)

		// Creates label for player Mana
		player1Mana = new Label("", BaseGame.battleLabelStyle);
		player1Mana.setColor(Color.WHITE);
		player1Mana.setPosition(TilemapActor.windowWidth / 6, TilemapActor.windowHeight - 50);
		uiStage.addActor(player1Mana);

		player2Mana = new Label("", BaseGame.battleLabelStyle);
		player2Mana.setColor(Color.WHITE);
		player2Mana.setPosition(TilemapActor.windowWidth / 6, TilemapActor.windowHeight - 75);
		uiStage.addActor(player2Mana);

		player3Mana = new Label("", BaseGame.battleLabelStyle);
		player3Mana.setColor(Color.WHITE);
		player3Mana.setPosition(TilemapActor.windowWidth / 6, TilemapActor.windowHeight - 100);
		uiStage.addActor(player3Mana);

		player4Mana = new Label("", BaseGame.battleLabelStyle);
		player4Mana.setColor(Color.WHITE);
		player4Mana.setPosition(TilemapActor.windowWidth / 6, TilemapActor.windowHeight - 125);
		uiStage.addActor(player4Mana);

		// Creates label for player Damage
		player1Dmg = new Label("", BaseGame.battleLabelStyle);
		player1Dmg.setColor(Color.YELLOW);
		player1Dmg.setPosition(TilemapActor.windowWidth / 4, TilemapActor.windowHeight - 50);
		uiStage.addActor(player1Dmg);

		player2Dmg = new Label("", BaseGame.battleLabelStyle);
		player2Dmg.setColor(Color.YELLOW);
		player2Dmg.setPosition(TilemapActor.windowWidth / 4, TilemapActor.windowHeight - 75);
		uiStage.addActor(player2Dmg);

		player3Dmg = new Label("", BaseGame.battleLabelStyle);
		player3Dmg.setColor(Color.YELLOW);
		player3Dmg.setPosition(TilemapActor.windowWidth / 4, TilemapActor.windowHeight - 100);
		uiStage.addActor(player3Dmg);

		player4Dmg = new Label("", BaseGame.battleLabelStyle);
		player4Dmg.setColor(Color.YELLOW);
		player4Dmg.setPosition(TilemapActor.windowWidth / 4, TilemapActor.windowHeight - 125);
		uiStage.addActor(player4Dmg);

		player1Name = new Label("", BaseGame.battleLabelStyle);
		player1Name.setColor(Color.BLACK);
		player1Name.setPosition(TilemapActor.windowWidth / 25, TilemapActor.windowHeight - 50);
		uiStage.addActor(player1Name);

		player2Name = new Label("", BaseGame.battleLabelStyle);
		player2Name.setColor(Color.BLACK);
		player2Name.setPosition(TilemapActor.windowWidth / 25, TilemapActor.windowHeight - 75);
		uiStage.addActor(player2Name);

		player3Name = new Label("", BaseGame.battleLabelStyle);
		player3Name.setColor(Color.BLACK);
		player3Name.setPosition(TilemapActor.windowWidth / 25, TilemapActor.windowHeight - 100);
		uiStage.addActor(player3Name);

		player4Name = new Label("", BaseGame.battleLabelStyle);
		player4Name.setColor(Color.BLACK);
		player4Name.setPosition(TilemapActor.windowWidth / 25, TilemapActor.windowHeight - 125);
		uiStage.addActor(player4Name);

		enemy1Hp = new Label("", BaseGame.battleLabelStyle);
		enemy1Hp.setColor(Color.RED);
		enemy1Hp.setPosition(TilemapActor.windowWidth / 12 * 10, TilemapActor.windowHeight - 50);
		uiStage.addActor(enemy1Hp);

		enemy2Hp = new Label("", BaseGame.battleLabelStyle);
		enemy2Hp.setColor(Color.RED);
		enemy2Hp.setPosition(TilemapActor.windowWidth / 12 * 10, TilemapActor.windowHeight - 75);
		uiStage.addActor(enemy2Hp);

		enemy3Hp = new Label("", BaseGame.battleLabelStyle);
		enemy3Hp.setColor(Color.RED);
		enemy3Hp.setPosition(TilemapActor.windowWidth / 12 * 10, TilemapActor.windowHeight - 100);
		uiStage.addActor(enemy3Hp);

		// Player text buttons
		player1Button = new TextButton("Dave", BaseGame.battleTextButtonStyle);
		player1Button.setPosition(200, 180);
		;
		uiStage.addActor(player1Button);

		player2Button = new TextButton("Lucy", BaseGame.battleTextButtonStyle);
		player2Button.setPosition(200, 130);
		player2Button.setVisible(false);

		uiStage.addActor(player2Button);

		player3Button = new TextButton("Yennefer", BaseGame.battleTextButtonStyle);
		player3Button.setPosition(200, 80);
		player3Button.setVisible(false);

		uiStage.addActor(player3Button);

		player4Button = new TextButton("Hank", BaseGame.battleTextButtonStyle);
		player4Button.setPosition(200, 30);
		player4Button.setVisible(false);

		uiStage.addActor(player4Button);

		// Action buttons - Attack
		attackButtonStyle = new ButtonStyle();

		Texture attackButtonTex = new Texture(Gdx.files.internal("battleScreen/attackButton.png"));
		TextureRegion attackButtonRegion = new TextureRegion(attackButtonTex);
		attackButtonStyle.up = new TextureRegionDrawable(attackButtonRegion);

		attackButton = new Button(attackButtonStyle);
		attackButton.setPosition(384, 110);
		uiStage.addActor(attackButton);

		// Action button - Skill
		skillButtonStyle = new ButtonStyle();

		Texture skillButtonTex = new Texture(Gdx.files.internal("battleScreen/skillButton.png"));
		TextureRegion skillButtonRegion = new TextureRegion(skillButtonTex);
		skillButtonStyle.up = new TextureRegionDrawable(skillButtonRegion);

		skillButton = new Button(skillButtonStyle);
		skillButton.setPosition(384, 60);
		uiStage.addActor(skillButton);

		// Action button - Block
		blockButtonStyle = new ButtonStyle();

		Texture blockButtonTex = new Texture(Gdx.files.internal("battleScreen/blockButton.png"));
		TextureRegion blockButtonRegion = new TextureRegion(blockButtonTex);
		blockButtonStyle.up = new TextureRegionDrawable(blockButtonRegion);

		blockButton = new Button(blockButtonStyle);
		blockButton.setPosition(384, 10);
		uiStage.addActor(blockButton);

		// Skill buttons
		executeSkill = new TextButton("Execute", BaseGame.battleTextButtonStyle);
		executeSkill.setPosition(384, 100);
		executeSkill.setVisible(false);
		uiStage.addActor(executeSkill);
		
		frostSkill = new TextButton("Frost", BaseGame.battleTextButtonStyle);
		frostSkill.setPosition(384, 100);
		frostSkill.setVisible(false);
		uiStage.addActor(frostSkill);
		
		fireSkill = new TextButton("Fire", BaseGame.battleTextButtonStyle);
		fireSkill.setPosition(384, 100);
		fireSkill.setVisible(false);
		fireSkill.addActor(fireSkill);

		healSkill = new TextButton("Heal", BaseGame.battleTextButtonStyle);
		healSkill.setPosition(384, 100);
		healSkill.setVisible(false);
		uiStage.addActor(healSkill);

		backButton = new TextButton("Back", BaseGame.battleTextButtonStyle);
		backButton.setPosition(600, 100);
		backButton.setColor(Color.BLACK);
		backButton.setVisible(false);
		uiStage.addActor(backButton);

		// Enemy text buttons
		enemy1 = new TextButton("Enemy 1", BaseGame.battleTextButtonStyle);
		enemy1.setPosition(960 - enemy1.getWidth(), 180);
		enemy1.setColor(Color.RED);
		uiStage.addActor(enemy1);

		enemy2 = new TextButton("Enemy 2", BaseGame.battleTextButtonStyle);
		enemy2.setPosition(960 - enemy2.getWidth(), 100);
		enemy2.setColor(Color.RED);
		enemy2.setVisible(true);
		uiStage.addActor(enemy2);

		enemy3 = new TextButton("Enemy 3", BaseGame.battleTextButtonStyle);
		enemy3.setPosition(960 - enemy3.getWidth(), 20);
		enemy3.setColor(Color.RED);
		enemy3.setVisible(true);
		uiStage.addActor(enemy3);

		
		if (battleParty.size() < 2) {
			healthBarFore = new HealthBar(0, 0, mainStage);
			healthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			healthBarFore.setPosition(TilemapActor.windowWidth / 8, TilemapActor.windowHeight - 245);
			healthBarFore.setSize(battleParty.get(0).getHp() * 2, 10);

		} else if (battleParty.size() == 2) {
			healthBarFore = new HealthBar(0, 0, mainStage);
			healthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			healthBarFore.setPosition(TilemapActor.windowWidth / 7, TilemapActor.windowHeight - 220);
			healthBarFore.setSize(battleParty.get(0).getHp() * 2, 10);

			lucyHealthBarFore = new HealthBar(0, 0, mainStage);
			lucyHealthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			lucyHealthBarFore.setPosition(TilemapActor.windowWidth / 13, TilemapActor.windowHeight - 280);
			lucyHealthBarFore.setSize(battleParty.get(1).getHp() * 2, 10);

		}
		if (battleParty.size() == 3) {
			healthBarFore = new HealthBar(0, 0, mainStage);
			healthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			healthBarFore.setPosition(TilemapActor.windowWidth / 7, TilemapActor.windowHeight - 220);
			healthBarFore.setSize(battleParty.get(0).getHp() * 2, 10);

			lucyHealthBarFore = new HealthBar(0, 0, mainStage);
			lucyHealthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			lucyHealthBarFore.setPosition(TilemapActor.windowWidth / 13, TilemapActor.windowHeight - 280);
			lucyHealthBarFore.setSize(battleParty.get(1).getHp() * 2, 10);

			yenneferHealthBarFore = new HealthBar(0, 0, mainStage);
			yenneferHealthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			yenneferHealthBarFore.setPosition(TilemapActor.windowWidth / 7, TilemapActor.windowHeight - 220);
			yenneferHealthBarFore.setSize(battleParty.get(2).getHp() * 2, 10);

		}
		if (battleParty.size() == 4) {
			healthBarFore = new HealthBar(0, 0, mainStage);
			healthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			healthBarFore.setPosition(TilemapActor.windowWidth / 7, TilemapActor.windowHeight - 220);
			healthBarFore.setSize(battleParty.get(0).getHp() * 2, 10);

			lucyHealthBarFore = new HealthBar(0, 0, mainStage);
			lucyHealthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			lucyHealthBarFore.setPosition(TilemapActor.windowWidth / 13, TilemapActor.windowHeight - 280);
			lucyHealthBarFore.setSize(battleParty.get(1).getHp() * 2, 10);

			yenneferHealthBarFore = new HealthBar(0, 0, mainStage);
			yenneferHealthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			yenneferHealthBarFore.setPosition(TilemapActor.windowWidth / 19, TilemapActor.windowHeight - 220);
			yenneferHealthBarFore.setSize(battleParty.get(2).getHp() * 2, 10);

			hankHealthBarFore = new HealthBar(0, 0, mainStage);
			hankHealthBarFore.loadTexture("battleScreen/blood_red_bar.png");
			hankHealthBarFore.setPosition(TilemapActor.windowWidth / 25, TilemapActor.windowHeight - 220);
			hankHealthBarFore.setSize(battleParty.get(3).getHp() * 2, 10);
		}

		// Player 1 button listeners
		player1Button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (p1But == true) {
					skillButton.setVisible(true);
					blockButton.setVisible(true);
					attackButton.setVisible(true);
					backButton.setVisible(false);
					executeSkill.setVisible(false);
					executeSkill.setColor(Color.WHITE);
					attBut = true;
					skillBut = true;
					blockBut = true;
					execBut = true;
					backBut = true;
					p1But = false;
					dialogueBox.setVisible(false);
					checkattack();
					checkblock();
					checkskill();
					checkexec();
					checkback();
					checkp1();
				} else {
					checkp1();
				}

				skillButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p1But == true) {
							execBut = true;
							blockBut = true;
							attBut = true;
							checkexec();
							checkblock();
							checkattack();
							checkskill();
							blockButton.setVisible(false);
							skillButton.setVisible(false);
							attackButton.setVisible(false);
							executeSkill.setVisible(true);
							backButton.setVisible(true);
						}

						executeSkill.addListener(new ClickListener() {
							@Override
							public void clicked(InputEvent event, float x, float y) {
								if (execBut == false) {
									if (battleParty.get(0).getMana() > 0) {
										checkexec();
									} else {
										dialogueBox.setText("Not enough Mana");
										execBut = true;
										checkexec();
									}
									dialogueBox.setVisible(true);
									dialogueBox.setText("Deals " + battleParty.get(0).getDamage() * 3 / 2
											+ " damage or defeats enemy below " + battleParty.get(0).getDamage() * 2
											+ " health");
								} else {
									checkexec();
									dialogueBox.setVisible(false);
								}
							}
						});
					}
				});

				attackButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p1But == true) {
							blockBut = true;
							skillBut = true;
							checkblock();
							checkskill();
							checkattack();
						}
					}
				});

				blockButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p1But == true) {
							attBut = true;
							skillBut = true;
							checkblock();
							checkskill();
							checkattack();
							battleParty.get(0).setBlocking(true);
						}
					}
				});

				backButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p1But == true) {
							skillBut = true;
							execBut = true;
							dialogueBox.setVisible(false);
							blockButton.setVisible(true);
							skillButton.setVisible(true);
							attackButton.setVisible(true);
							executeSkill.setVisible(false);
							backButton.setVisible(false);
							checkskill();
							checkexec();
							checkback();
						}
					}
				});
			}
		});

		
		
		
		// Player 2 button listeners
		if (battleParty.size() >= 2) {
			player2Button.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					if (p2But == true) {
						p1But = true;
						checkp1();
						skillButton.setVisible(true);
						blockButton.setVisible(true);
						attackButton.setVisible(true);
						backButton.setVisible(false);
						executeSkill.setVisible(false);
						healSkill.setVisible(false);
						executeSkill.setColor(Color.WHITE);
						attBut = true;
						skillBut = true;
						blockBut = true;
						execBut = true;
						backBut = true;
						p2But = false;
						dialogueBox.setVisible(false);
						checkattack();
						checkblock();
						checkskill();
						checkexec();
						checkback();
						checkp2();
					} else {
						checkp2();
					}

					skillButton.addListener(new ClickListener() {
						@Override
						public void clicked(InputEvent event, float x, float y) {
							if (p2But == true) {
								blockBut = true;
								attBut = true;
								checkblock();
								checkattack();
								checkskill();
								blockButton.setVisible(false);
								skillButton.setVisible(false);
								attackButton.setVisible(false);
								backButton.setVisible(true);
								healSkill.setVisible(true);
								
								dialogueBox.setVisible(true);
								dialogueBox.setText("Heal a player for " + battleParty.get(1).getHeal() + " health");
								
									
							}

							healSkill.addListener(new ClickListener() {
								@Override
								public void clicked(InputEvent event, float x, float y) {
										if (battleParty.get(1).getMana() > 0) {
											Heal heal = new Heal(0,0,mainStage);
											heal.centerAtPosition(480, 260);
											for (PlayableCharacter pc : battleParty) {
												pc.setHp(pc.getHp() + 10);
											}
											blockButton.setVisible(false);
											skillButton.setVisible(false);
											attackButton.setVisible(false);
											dialogueBox.setVisible(false);
											healSkill.setVisible(false);
											backButton.setVisible(false);
										} else {
											dialogueBox.setText("Not enough Mana");	
										}
									}
								
							});
						}
					});

					attackButton.addListener(new ClickListener() {
						@Override
						public void clicked(InputEvent event, float x, float y) {
							if (p2But == true) {
								blockBut = true;
								skillBut = true;
								checkblock();
								checkskill();
								checkattack();
							}
						}
					});

					blockButton.addListener(new ClickListener() {
						@Override
						public void clicked(InputEvent event, float x, float y) {
							if (p2But == true) {
								attBut = true;
								skillBut = true;
								checkblock();
								checkskill();
								checkattack();
								battleParty.get(1).setBlocking(true);
							}
						}
					});

					backButton.addListener(new ClickListener() {
						@Override
						public void clicked(InputEvent event, float x, float y) {
							if (p2But == true) {
								skillBut = true;
								healSkill.setVisible(false);
								dialogueBox.setVisible(false);
								blockButton.setVisible(true);
								skillButton.setVisible(true);
								attackButton.setVisible(true);
								backButton.setVisible(false);
								checkskill();
								checkback();
							}
						}
					});
				}
			});
		}

		// Player 3 button listeners
		player3Button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (p3But == true) {
					p1But = true;
					checkp1();
					p2But = true;
					checkp2();
					skillButton.setVisible(true);
					blockButton.setVisible(true);
					attackButton.setVisible(true);
					backButton.setVisible(false);
					executeSkill.setVisible(false);
					executeSkill.setColor(Color.WHITE);
					attBut = true;
					skillBut = true;
					blockBut = true;
					execBut = true;
					backBut = true;
					p3But = false;
					dialogueBox.setVisible(false);
					checkattack();
					checkblock();
					checkskill();
					checkexec();
					checkback();
					checkp3();
				} else {
					checkp3();
				}

				skillButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p3But == true) {
							frostBut = true;
							blockBut = true;
							attBut = true;
							checkblock();
							checkattack();
							checkskill();
							checkfrost();
							blockButton.setVisible(false);
							skillButton.setVisible(false);
							attackButton.setVisible(false);
							frostSkill.setVisible(true);
							backButton.setVisible(true);
						}

						frostSkill.addListener(new ClickListener() {
							@Override
							public void clicked(InputEvent event, float x, float y) {
								if (frostBut == false) {
									if (battleParty.get(2).getMana() > 0) {
										 checkfrost();
									} else {
										dialogueBox.setText("Not enough Mana");
										frostBut = true;
										checkfrost();
									}
									dialogueBox.setVisible(true);
									dialogueBox.setText("Deals " + battleParty.get(2).getDamage() * 3 / 2
											+ " damage or defeats enemy below " + battleParty.get(2).getDamage() * 2
											+ " health");
								} else {
									checkfrost();
									dialogueBox.setVisible(false);
								}
							}
						});
					}
				});

				attackButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p3But == true) {
							blockBut = true;
							skillBut = true;
							checkblock();
							checkskill();
							checkattack();
						}
					}
				});

				blockButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p3But == true) {
							attBut = true;
							skillBut = true;
							checkblock();
							checkskill();
							checkattack();
							battleParty.get(2).setBlocking(true);
						}
					}
				});

				backButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p3But == true) {
							skillBut = true;
							execBut = true;
							dialogueBox.setVisible(false);
							blockButton.setVisible(true);
							skillButton.setVisible(true);
							attackButton.setVisible(true);
							executeSkill.setVisible(false);
							backButton.setVisible(false);
							checkskill();
							checkexec();
							checkback();
						}
					}
				});
			}
		});

		// Player 4 button listeners
		player4Button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (p4But == true) {
					p1But = true;
					checkp1();
					p2But = true;
					checkp2();
					p3But = true;
					checkp3();
					skillButton.setVisible(true);
					blockButton.setVisible(true);
					attackButton.setVisible(true);
					backButton.setVisible(false);
					executeSkill.setVisible(false);
					executeSkill.setColor(Color.WHITE);
					attBut = true;
					skillBut = true;
					blockBut = true;
					execBut = true;
					backBut = true;
					p4But = false;
					dialogueBox.setVisible(false);
					checkattack();
					checkblock();
					checkskill();
					checkexec();
					checkback();
					checkp4();
				} else {
					checkp4();
				}

				skillButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p4But == true) {
							fireBut = true;
							blockBut = true;
							attBut = true;
							checkfire();
							checkblock();
							checkattack();
							checkskill();
							blockButton.setVisible(false);
							skillButton.setVisible(false);
							attackButton.setVisible(false);
							fireSkill.setVisible(true);
							backButton.setVisible(true);
						}

						fireSkill.addListener(new ClickListener() {
							@Override
							public void clicked(InputEvent event, float x, float y) {
								if (fireBut == false) {
									if (battleParty.get(3).getMana() > 0) {
										checkfire();
									} else {
										dialogueBox.setText("Not enough Mana");
										fireBut = true;
										checkfire();
									}
									dialogueBox.setVisible(true);
									dialogueBox.setText("Deals " + battleParty.get(2).getDamage() * 3 / 2
											+ " damage or defeats enemy below " + battleParty.get(3).getDamage() * 2
											+ " health");
								} else {
									checkfire();
									dialogueBox.setVisible(false);
								}
							}
						});
					}
				});

				attackButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p4But == true) {
							blockBut = true;
							skillBut = true;
							checkblock();
							checkskill();
							checkattack();
						}
					}
				});

				blockButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p4But == true) {
							attBut = true;
							skillBut = true;
							checkblock();
							checkskill();
							checkattack();
							battleParty.get(3).setBlocking(true);
						}
					}
				});

				backButton.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p4But == true) {
							skillBut = true;
							execBut = true;
							dialogueBox.setVisible(false);
							blockButton.setVisible(true);
							skillButton.setVisible(true);
							attackButton.setVisible(true);
							executeSkill.setVisible(false);
							backButton.setVisible(false);
							checkskill();
							checkexec();
							checkback();
						}
					}
				});
			}
		});

		// Enemy 1 button listeners
		enemy1.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (p1But == true && attBut == true) {
					enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(0).getDamage());
					checkp1();
					checkattack();
					turnOver();
				}
				if (execBut == true && battleParty.get(0).getMana() > 0) {
					Punch punch = new Punch(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
							mainStage);
					if (enemy.getE1Hp() <= battleParty.get(0).getDamage() * 2) {
						enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(0).getDamage() * 2);
					} else {
						enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(0).getDamage() * 3 / 2);
					}

					execBut = true;
					p1But = true;
					attBut = true;
					blockBut = true;
					skillBut = true;
					backBut = true;
					checkexec();
					checkp1();
					checkblock();
					checkattack();
					checkskill();
					checkback();
					executeSkill.setVisible(false);
					backButton.setVisible(false);
					attackButton.setVisible(true);
					skillButton.setVisible(true);
					blockButton.setVisible(true);
					battleParty.get(0).setMana(battleParty.get(0).getMana() - 1);
					dialogueBox.setVisible(false);
					turnOver();
				}
				enemyDeath();

				if (allDead()) {
					playerDeath();
					gameOver();
				}
			}

		});

		// Enemy 2 button listeners
		enemy2.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (p1But == true && attBut == true) {
					enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(0).getDamage());
					checkp1();
					checkattack();
					turnOver();
				}
				if (execBut == true && battleParty.get(0).getMana() > 0) {
					Punch punch = new Punch(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
							mainStage);
					if (enemy.getE2Hp() <= battleParty.get(0).getDamage() * 2) {
						enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(0).getDamage() * 2);
					} else {
						enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(0).getDamage() * 3 / 2);
					}

					execBut = true;
					p1But = true;
					attBut = true;
					blockBut = true;
					skillBut = true;
					backBut = true;
					checkexec();
					checkp1();
					checkblock();
					checkattack();
					checkskill();
					checkback();
					executeSkill.setVisible(false);
					backButton.setVisible(false);
					attackButton.setVisible(true);
					skillButton.setVisible(true);
					blockButton.setVisible(true);
					battleParty.get(0).setMana(battleParty.get(0).getMana() - 1);
					dialogueBox.setVisible(false);
					turnOver();
				}
				enemyDeath();

				if (allDead()) {
					playerDeath();
					gameOver();
				}
			}
		});

		// Enemy 3 button listeners
		enemy3.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (p1But == true && attBut == true) {
					enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(0).getDamage());
					checkp1();
					checkattack();
					turnOver();
				}
				if (execBut == true && battleParty.get(0).getMana() > 0) {
					Punch punch = new Punch(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
							mainStage);
					if (enemy.getE3Hp() <= battleParty.get(0).getDamage() * 2) {
						enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(0).getDamage() * 2);
					} else {
						enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(0).getDamage() * 3 / 2);
					}

					execBut = true;
					p1But = true;
					attBut = true;
					blockBut = true;
					skillBut = true;
					backBut = true;
					checkexec();
					checkp1();
					checkblock();
					checkattack();
					checkskill();
					checkback();
					executeSkill.setVisible(false);
					backButton.setVisible(false);
					attackButton.setVisible(true);
					skillButton.setVisible(true);
					blockButton.setVisible(true);
					battleParty.get(0).setMana(battleParty.get(0).getMana() - 1);
					dialogueBox.setVisible(false);
					turnOver();
				}
				enemyDeath();

				if (allDead()) {
					playerDeath();
					gameOver();
				}
			}
		});



		
		// Enemy 1 button listeners for 2 players
				enemy1.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p2But == true && attBut == true) {
							enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(1).getDamage());
							checkp2();
							checkattack();
							turnOver();
						}
						if (battleParty.get(1).getMana() > 0) {
							Heal heal = new Heal(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE1Hp() <= battleParty.get(1).getDamage() * 2) {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(1).getDamage() * 2);
							} else {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(1).getDamage() * 3 / 2);
							}

							p2But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkp2();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							healSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(1).setMana(battleParty.get(1).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}

				});

				// Enemy 2 button listeners
				enemy2.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p2But == true && attBut == true) {
							enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(1).getDamage());
							checkp2();
							checkattack();
							turnOver();
						}
						if (battleParty.get(1).getMana() > 0) {
							Heal heal = new Heal(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE2Hp() <= battleParty.get(1).getDamage() * 2) {
								enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(1).getDamage() * 2);
							} else {
								enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(1).getDamage() * 3 / 2);
							}

							p2But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkp2();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							healSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(1).setMana(battleParty.get(1).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}
				});
				
				
				
				// Enemy 3 button listeners
				enemy3.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p2But == true && attBut == true) {
							enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(1).getDamage());
							checkp2();
							checkattack();
							turnOver();
						}
						if (battleParty.get(1).getMana() > 0) {
							Heal heal = new Heal(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE3Hp() <= battleParty.get(1).getDamage() * 2) {
								enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(1).getDamage() * 2);
							} else {
								enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(1).getDamage() * 3 / 2);
							}

							p2But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkp2();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							healSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(1).setMana(battleParty.get(1).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}
				});
		
		
				
				
				
				
				
				
				
				
				
				// Enemy 1 button listeners
				enemy1.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p3But == true && attBut == true) {
							enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(2).getDamage());
							checkp3();
							checkattack();
							turnOver();
						}
						if (frostBut == true && battleParty.get(2).getMana() > 0) {
							Frost frost = new Frost(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE1Hp() <= battleParty.get(2).getDamage() * 2) {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(2).getDamage() * 2);
							} else {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(2).getDamage() * 3 / 2);
							}

							frostBut = true;
							p3But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkfrost();
							checkp3();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							frostSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(2).setMana(battleParty.get(2).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}

				});

				// Enemy 2 button listeners
				enemy2.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p3But == true && attBut == true) {
							enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(2).getDamage());
							checkp3();
							checkattack();
							turnOver();
						}
						if (frostBut == true && battleParty.get(2).getMana() > 0) {
							Frost frost = new Frost(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE1Hp() <= battleParty.get(2).getDamage() * 2) {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(2).getDamage() * 2);
							} else {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(2).getDamage() * 3 / 2);
							}

							frostBut = true;
							p3But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkfrost();
							checkp3();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							frostSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(2).setMana(battleParty.get(2).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}
				});

				// Enemy 3 button listeners
				enemy3.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p3But == true && attBut == true) {
							enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(2).getDamage());
							checkp3();
							checkattack();
							turnOver();
						}
						if (frostBut == true && battleParty.get(2).getMana() > 0) {
							Frost frost = new Frost(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE1Hp() <= battleParty.get(2).getDamage() * 2) {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(2).getDamage() * 2);
							} else {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(2).getDamage() * 3 / 2);
							}

							frostBut = true;
							p3But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkfrost();
							checkp3();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							frostSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(2).setMana(battleParty.get(2).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}
				});	
				
				
				
				
				
				
				
				
				// Enemy 1 button listeners
				enemy1.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p4But == true && attBut == true) {
							enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(3).getDamage());
							checkp4();
							checkattack();
							turnOver();
						}
						if (fireBut == true && battleParty.get(3).getMana() > 0) {
							Fireball fireball = new Fireball(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE1Hp() <= battleParty.get(3).getDamage() * 2) {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(3).getDamage() * 2);
							} else {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(3).getDamage() * 3 / 2);
							}

							fireBut = true;
							p4But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkfire();
							checkp4();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							fireSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(3).setMana(battleParty.get(3).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}

				});

				// Enemy 2 button listeners
				enemy2.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p4But == true && attBut == true) {
							enemy.setE2Hp(enemy.getE2Hp() - battleParty.get(3).getDamage());
							checkp4();
							checkattack();
							turnOver();
						}
						if (fireBut == true && battleParty.get(3).getMana() > 0) {
							Fireball fireball = new Fireball(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE1Hp() <= battleParty.get(3).getDamage() * 2) {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(3).getDamage() * 2);
							} else {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(3).getDamage() * 3 / 2);
							}

							fireBut = true;
							p4But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkfire();
							checkp4();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							fireSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(3).setMana(battleParty.get(3).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}
				});

				// Enemy 3 button listeners
				enemy3.addListener(new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (p4But == true && attBut == true) {
							enemy.setE3Hp(enemy.getE3Hp() - battleParty.get(3).getDamage());
							checkp4();
							checkattack();
							turnOver();
						}
						if (fireBut == true && battleParty.get(3).getMana() > 0) {
							Fireball fireball = new Fireball(TilemapActor.windowWidth / 2 - 100, TilemapActor.windowHeight / 2,
									mainStage);
							if (enemy.getE1Hp() <= battleParty.get(3).getDamage() * 2) {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(3).getDamage() * 2);
							} else {
								enemy.setE1Hp(enemy.getE1Hp() - battleParty.get(3).getDamage() * 3 / 2);
							}

							fireBut = true;
							p4But = true;
							attBut = true;
							blockBut = true;
							skillBut = true;
							backBut = true;
							checkfire();
							checkp4();
							checkblock();
							checkattack();
							checkskill();
							checkback();
							fireSkill.setVisible(false);
							backButton.setVisible(false);
							attackButton.setVisible(true);
							skillButton.setVisible(true);
							blockButton.setVisible(true);
							battleParty.get(3).setMana(battleParty.get(3).getMana() - 1);
							dialogueBox.setVisible(false);
							turnOver();
						}
						enemyDeath();

						if (allDead()) {
							playerDeath();
							gameOver();
						}
					}
				});
				
				
				
				
				
		
	}

	@Override
	public void update(float dt) {

		if (BaseGame.activeScreen == "club1") {
			// Monster, one player
			if (enemy.getID() == 1) {
				BattleScreenActors.loadTexture("battleScreen/battle1.png");
			}

			// 3 monsters, one player
			if (enemy.getID() == 2) {
				BattleScreenActors.loadTexture("battleScreen/battle2.png");
			}

			// Boss monster
			if (enemy.getID() == 3) {
				BattleScreenActors.loadTexture("battleScreen/battle5.png");
			}
		}

		if (BaseGame.activeScreen == "club2") {
			// Skeleton
			if (enemy.getID() == 4) {
				BattleScreenActors.loadTexture("battleScreen/battle6.png");
			}
			if (enemy.getID() == 5) {
				BattleScreenActors.loadTexture("battleScreen/battle7.png");
			}
		}

		if (BaseGame.activeScreen == "club3") {
			if (enemy.getID() == 2) {
				BattleScreenActors.loadTexture("battleScreen/battle10.png");
			}

			if (enemy.getID() == 1) {
				BattleScreenActors.loadTexture("battleScreen/battle9.png");
			}
		}
		if (BaseGame.activeScreen == "club4") {
			if (enemy.getID() == 7) {
				BattleScreenActors.loadTexture("battleScreen/battle14.png");
			}

			if (enemy.getID() == 1) {
				BattleScreenActors.loadTexture("battleScreen/battle16.png");
			}
			if (enemy.getID() == 6) {
				BattleScreenActors.loadTexture("battleScreen/battle12.png");
			}

		}
		if (BaseGame.activeScreen == "club5") {
			if (enemy.getID() == 1) {
				BattleScreenActors.loadTexture("battleScreen/battle17.png");
			}
			if (enemy.getID() == 2) {
				BattleScreenActors.loadTexture("battleScreen/battle16.png");
			}
			if (enemy.getID() == 3) {
				BattleScreenActors.loadTexture("battleScreen/battle18.png");
			}
			if (enemy.getID() == 6) {
				BattleScreenActors.loadTexture("battleScreen/battle19.png");
			}
			if (enemy.getID() == 7) {
				BattleScreenActors.loadTexture("battleScreen/battle14.png");
			}
			if (enemy.getID() == 8) {
				BattleScreenActors.loadTexture("battleScreen/battle13.png");
			}

		}

		if (BaseGame.activeScreen == "club6") {
			if (enemy.getID() == 6) {
				BattleScreenActors.loadTexture("battleScreen/battle19.png");
			}
			if (enemy.getID() == 7) {
				BattleScreenActors.loadTexture("battleScreen/battle14.png");
			}
			if (enemy.getID() == 8) {
				BattleScreenActors.loadTexture("battleScreen/battle13.png");
			}
			if (enemy.getID() == 9) {
				BattleScreenActors.loadTexture("battleScreen/battle20.png");
			}

		}

		// Updates player stats enemy health points constantly, once enemy health is 0
		// or below; remove enemy and set screen back to game screen
		player1Name.setText("Dave");
		player1Mana.setText("Mana:" + battleParty.get(0).getMana());
		player1Dmg.setText("Attack Damage:" + battleParty.get(0).getDamage());

		if (battleParty.size() >= 2) {
			player2Button.setVisible(true);
			player2Name.setText("Lucy");
			player2Mana.setText("Mana:" + battleParty.get(1).getMana());
			player2Dmg.setText("Attack Damage:" + battleParty.get(1).getDamage());
		}

		if (battleParty.size() >= 3) {
			player3Button.setVisible(true);
			player3Name.setText("yennefer");
			player3Mana.setText("Mana:" + battleParty.get(2).getMana());
			player3Dmg.setText("Attack Damage:" + battleParty.get(2).getDamage());
		}

		if (battleParty.size() >= 4) {
			player4Button.setVisible(true);
			player4Name.setText("Hank");
			player4Mana.setText("Mana:" + battleParty.get(3).getMana());
			player4Dmg.setText("Attack Damage:" + battleParty.get(3).getDamage());
		}

		enemy1Hp.setText("Enemy1 HP:" + enemy.getE1Hp());

		if (enemy.getE2Hp() > 0) {
			enemy2Hp.setText("Enemy2 HP:" + enemy.getE2Hp());
		}

		if (enemy.getE3Hp() > 0) {
			enemy3Hp.setText("Enemy3 HP:" + enemy.getE3Hp());
		}
	}

	public void turnOver() {
		// Random damage in bounds of min - max from tiledmap and random player target
		randomDmg = new Random();
		int dmg = randomDmg.nextInt(enemy.getMaxDmg() - enemy.getMinDmg() + 1) + enemy.getMinDmg();

		randomPlayerTarget = new Random();
		int p = randomPlayerTarget.nextInt(battleParty.size()) + 1;

		float damage = (float) dmg;

		// enemy attack animations
		if (p1But == true && blockBut == true) {
			battleParty.get(0).setHp(battleParty.get(0).getHp() - dmg / 2);
			for (float x = 0; x <= (damage / 2); x = x + 0.01f) {
				healthBarFore.setSize((stamina - x), 10);
			}
			stamina = stamina - (damage / 2);
		} else if (p2But == true && blockBut == true) {
			battleParty.get(1).setHp(battleParty.get(1).getHp() - dmg / 2);
			for (float x = 0; x <= (damage / 2); x = x + 0.01f) {
				lucyHealthBarFore.setSize((stamina2 - x), 10);
			}
			stamina2 = stamina2 - (damage / 2);
		} else if (p3But == true && blockBut == true) {
			battleParty.get(2).setHp(battleParty.get(2).getHp() - dmg / 2);
			for (float x = 0; x <= (damage / 2); x = x + 0.01f) {
				yenneferHealthBarFore.setSize((stamina3 - x), 10);
			}
			stamina3 = stamina3 - (damage / 2);
		} else if (p4But == true && blockBut == true) {
			battleParty.get(3).setHp(battleParty.get(3).getHp() - dmg);
			for (float x = 0; x <= damage; x = x + 0.01f) {
				hankHealthBarFore.setSize((stamina4 - x), 10);
			}
			stamina4 = stamina4 - damage;
		}

		if (battleParty.size() == 2) {
			if (battleParty.get(0).getAlive() == true) {
				if (p == 1 || p == 2 || battleParty.get(1).getAlive() == false) {
					if (battleParty.get(0).getBlocking() == true) {
						battleParty.get(0).setHp(battleParty.get(0).getHp() - dmg / 2);
						for (float x = 0; x <= (damage / 2); x = x + 0.01f) {
							healthBarFore.setSize((stamina - x), 10);
						}
						stamina = stamina - (damage / 2);
					} else {
						battleParty.get(0).setHp(battleParty.get(0).getHp() - dmg);
						for (float x = 0; x <= damage; x = x + 0.01f) {
							healthBarFore.setSize((stamina - x), 10);
						}
						stamina = stamina - damage;
					}
				}
			}

			if (battleParty.get(1).getAlive() == true) {
				if (p == 3 || p == 4 || battleParty.get(0).getAlive() == false) {
					if (battleParty.get(1).getBlocking() == true) {
						battleParty.get(1).setHp(battleParty.get(1).getHp() - dmg / 2);
						for (float x = 0; x <= (damage / 2); x = x + 0.01f) {
							lucyHealthBarFore.setSize((stamina2 - x), 10);
						}
						stamina2 = stamina2 - (damage / 2);
					} else if (battleParty.get(2).getBlocking() == true) {
						battleParty.get(2).setHp(battleParty.get(2).getHp() - dmg / 2);
						for (float x = 0; x <= (damage / 2); x = x + 0.01f) {
							yenneferHealthBarFore.setSize((stamina3 - x), 10);
						}
						stamina3 = stamina3 - (damage / 2);
					} else {
						battleParty.get(3).setHp(battleParty.get(3).getHp() - dmg);
						for (float x = 0; x <= damage; x = x + 0.01f) {
							hankHealthBarFore.setSize((stamina4 - x), 10);
						}
						stamina4 = stamina4 - damage;
					}
				}
			}
		}

		for (PlayableCharacter pc : battleParty) {
			if (pc.getHp() <= 0)
				pc.setAlive(false);
		}

		playerDeath();
		gameOver();

	}

	public void enemyDeath() {
		if (enemy.getE1Hp() <= 0) {
			enemy1Hp.setVisible(false);
			enemy1.setVisible(false);
		}

		if (enemy.getE2Hp() <= 0) {
			enemy2.setVisible(false);
			enemy2Hp.setVisible(false);
		}

		if (enemy.getE3Hp() <= 0) {
			enemy3.setVisible(false);
			enemy3Hp.setVisible(false);
		}

		if (enemy.getE1Hp() <= 0 && enemy.getE2Hp() <= 0 && enemy.getE3Hp() <= 0) {
			enemy.remove();
			FreshersEscapade.setActiveScreen(gameScreen);
		}
	}

	public void playerDeath() {
		if (battleParty.get(0).getHp() <= 0) {
			player1Name.setText("Dave: Knocked out");
			player1Button.setVisible(false);
			player1Button.remove();
			player1Mana.remove();
			player1Dmg.remove();
			battleParty.get(0).setAlive(false);
		}
		if (battleParty.size() >= 2) {
			if (battleParty.get(1).getHp() <= 0) {
				player2Name.setText("Lucy: Knocked out");
				player2Button.setVisible(false);
				player2Button.remove();
				player2Mana.remove();
				player2Dmg.remove();
				battleParty.get(1).setAlive(false);
			}
			if (battleParty.size() >= 3) {
				if (battleParty.get(2).getHp() <= 0) {
					player3Name.setText("Yennefer: Knocked out");
					player3Button.setVisible(false);
					player3Button.remove();
					player3Mana.remove();
					player3Dmg.remove();
					battleParty.get(2).setAlive(false);
				}
				if (battleParty.size() >= 4) {
					if (battleParty.get(3).getHp() <= 0) {
						player4Name.setText("Hank: Knocked out");
						player4Button.setVisible(false);
						player4Button.remove();
						player4Mana.remove();
						player4Dmg.remove();
						battleParty.get(3).setAlive(false);
					}
				}
			}
		}
	}

	public void gameOver() {
		// When player health is less than or equal to zero, switch to gameover screen
		if (allDead()) {
			FreshersEscapade.setActiveScreen(new GameoverScreen());
			BaseGame.activeScreen = "gameover";
		}
	}

	public boolean allDead() {

		int count = 0;

		for (PlayableCharacter pc : battleParty) {
			if (pc.getAlive() == false) {
				count++;
				if (count == battleParty.size()) {
					return true;
				}
			}
		}
		return false;
	}

	public void checkp1() {
		if (p1But == false) {
			player1Button.setColor(Color.GREEN);
			p1But = true;
		} else {
			player1Button.setColor(Color.WHITE);
			p1But = false;
		}
	}

	public void checkp2() {
		if (p2But == false) {
			player2Button.setColor(Color.GREEN);
			p2But = true;
		} else {
			player2Button.setColor(Color.WHITE);
			p2But = false;
		}
	}

	public void checkp3() {
		if (p3But == false) {
			player3Button.setColor(Color.GREEN);
			p3But = true;
		} else {
			player3Button.setColor(Color.WHITE);
			p3But = false;
		}
	}

	public void checkp4() {
		if (p4But == false) {
			player4Button.setColor(Color.GREEN);
			p4But = true;
		} else {
			player4Button.setColor(Color.WHITE);
			p4But = false;
		}
	}

	public void checkskill() {
		if (skillBut == false) {
			skillButton.setColor(Color.GREEN);
			skillBut = true;
		} else {
			skillButton.setColor(Color.WHITE);
			skillBut = false;
		}
	}

	public void checkattack() {
		if (attBut == false) {
			attackButton.setColor(Color.GREEN);
			attBut = true;
		} else {
			attackButton.setColor(Color.WHITE);
			attBut = false;
		}
	}

	public void checkblock() {
		if (blockBut == false) {
			blockButton.setColor(Color.GREEN);
			blockBut = true;
		} else {
			blockButton.setColor(Color.WHITE);
			blockBut = false;
		}
	}

	public void checkexec() {
		if (execBut == false) {
			executeSkill.setColor(Color.GREEN);
			execBut = true;
		} else {
			executeSkill.setColor(Color.WHITE);
			execBut = false;
		}
	}
	
	public void checkfrost() {
		if (frostBut == false) {
			frostSkill.setColor(Color.GREEN);
			frostBut = true;
		} else {
			frostSkill.setColor(Color.WHITE);
			frostBut = false;
		}
	}
	
	public void checkfire() {
		if (fireBut == false) {
			fireSkill.setColor(Color.GREEN);
			fireBut = true;
		} else {
			fireSkill.setColor(Color.WHITE);
			fireBut = false;
		}
	}

	public void checkback() {
		if (backBut == false) {
			backButton.setColor(Color.BLACK);
			backBut = true;
		} else {
			backButton.setColor(Color.BLACK);
			backBut = false;
		}
	}

}
