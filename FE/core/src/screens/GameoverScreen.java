package screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import actors.BaseActor;
import actors.TilemapActor;
import game.BaseGame;
import game.FreshersEscapade;

public class GameoverScreen extends BaseScreen {

	@Override
	public void initialize() {
		/* Initialises the gameover screen background image (from the assets folder) at the corresponding size. */
		BaseActor gameover = new BaseActor(0,0,mainStage);
		gameover.loadTexture("gameoverScreen/gameover.png");
		gameover.setSize(TilemapActor.windowWidth, TilemapActor.windowHeight);
		
		
		/* Creates a text button with the text Start using the styles defined in the base game class */
		TextButton restartButton = new TextButton( "Restart", BaseGame.textButtonStyle );
		
		// When restart button is clicked on, set screen to a new game screen (restart game)
		restartButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				BaseGame.activeScreen = "game";
				FreshersEscapade.setActiveScreen( new GameScreen() );
				
			}
		});
		
		uiTable.align(Align.center|Align.bottom);
		uiTable.padBottom(20);
		uiTable.add(restartButton);

	}
	
	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub

	}

}
