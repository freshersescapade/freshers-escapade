package game;

import screens.MenuScreen;

public class FreshersEscapade extends BaseGame{
	
	/* Sets screen to the menu screen. */
	@Override
	public void create() {
		super.create();
		setActiveScreen( new MenuScreen() );
		BaseGame.activeScreen = "Menu";
	}

}
