package game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import controllers.PartyController;
import screens.BaseScreen;

public abstract class BaseGame extends Game {
	
	private static BaseGame game; // Stores static reference to game object
	public static LabelStyle labelStyle; // How text will be look
	public static LabelStyle battleLabelStyle;
	public static String activeScreen;	// global string of active screen
	public static boolean club1Complete =  true;
	public static boolean club2Complete =  true;
	public static boolean openingFinished = true;
	public static boolean lucyFinished = true;
	public static TextButtonStyle textButtonStyle;	//How text buttons will look
	public static TextButtonStyle battleTextButtonStyle;
	public static boolean club3Complete = true;
	public static boolean club4Complete = true;
	public static boolean club5Complete = true;
	public static boolean club6Complete = false;
	public static PartyController partyController;
	
	/* Global reference of game object initialised here. */
	public BaseGame() {
		game = this;
		labelStyle = new LabelStyle();
		battleLabelStyle = new LabelStyle();
		partyController = new PartyController();
	}
	
	public void create() {
		/*
		 * InputMultiplexer is an InputProcessor containing a list of other InputProcessors.
		 * Centralises all the InputProcessors from various classes into one multiplexer.
		 */
		InputMultiplexer im = new InputMultiplexer();
		Gdx.input.setInputProcessor(im);
		
		// How the text will look by using an image of all characters required
		labelStyle.font = new BitmapFont(Gdx.files.internal("default.fnt"));
		battleLabelStyle.font = new BitmapFont(Gdx.files.internal("battleScreen/battleFont.fnt"));
		
		// How the buttons will look and how the text on them will look
		textButtonStyle = new TextButtonStyle();
		Texture buttonTex = new Texture( Gdx.files.internal("testButtonSmall2.png") );
		NinePatch buttonPatch = new NinePatch(buttonTex, 24,24,24,24);
		textButtonStyle.up = new NinePatchDrawable( buttonPatch );
		textButtonStyle.font = new BitmapFont(Gdx.files.internal("impact.fnt"));
		
		battleTextButtonStyle = new TextButtonStyle();
		Texture buttonTex2 = new Texture( Gdx.files.internal("battleScreen/battleButton.png") );
		NinePatch buttonPatch2 = new NinePatch(buttonTex2, 24,24,24,24);
		battleTextButtonStyle.up = new NinePatchDrawable( buttonPatch2 );
		battleTextButtonStyle.font = new BitmapFont(Gdx.files.internal("battleScreen/battleFont.fnt"));

	}
	
	// Sets the active screen
	public static void setActiveScreen(BaseScreen s) {
		game.setScreen(s);
	}
}
	

