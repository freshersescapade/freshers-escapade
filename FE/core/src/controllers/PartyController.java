package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import actors.PlayableCharacter;

public class PartyController {

	public ArrayList<PlayableCharacter> party = new ArrayList<PlayableCharacter>();
		
	public PartyController() {	}
	
	public void addMember(PlayableCharacter pc) {
		if(party.size() > 0) {
			ListIterator<PlayableCharacter> it = party.listIterator();
			if(it.hasNext()) {
				if(!pc.getName().equals(it.next().getName()))
					party.add(pc);
				}
			}
		else
			party.add(pc);
		
	}

	public PlayableCharacter getPC(int i) {
		return party.get(i);
	}
	
	public int size() {
		return party.size();
	}
}
