package controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

import game.BaseGame;

public class MusicController {
	
	private Music clubMusic;
	
	public MusicController() {	}
	
	public void setMusic(String musicName) {
		switch(musicName) {
		case ("club1") : { clubMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Lono.mp3")); } break;
		case ("club2") : { clubMusic = Gdx.audio.newMusic(Gdx.files.internal("music/waterwater.mp3")); } break;
		case ("club3") : { clubMusic = Gdx.audio.newMusic(Gdx.files.internal("music/rain.mp3")); } break;
		case ("club4") : { clubMusic = Gdx.audio.newMusic(Gdx.files.internal("music/MURKYYYY.mp3")); } break;
		case ("club5") : { clubMusic = Gdx.audio.newMusic(Gdx.files.internal("music/waterwater.mp3")); } break;
		case ("club6") : { clubMusic = Gdx.audio.newMusic(Gdx.files.internal("music/doot.mp3")); } break;
 		}
	}
	
	public void pause() {
		clubMusic.pause();
	}
	
	public void play() {
		if(BaseGame.activeScreen != "game" )
			clubMusic.play();
	}
	
	public void stop() {
		clubMusic.stop();
	}
	
	public void dispose() {
		clubMusic.dispose();
	}
	
	
	
}
