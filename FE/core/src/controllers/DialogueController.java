package controllers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

import actors.DialogueBox;
import dialogue.Dialogue;
import dialogue.DialogueNode;
import dialogue.DialogueTraverser;

public class DialogueController extends InputAdapter {

	private DialogueTraverser traverser;
	private DialogueBox dialogueBox;
	private boolean finished = false;
	
	public DialogueController(DialogueBox dBox) {
		dialogueBox = dBox;
	}
	
	@Override
	public boolean keyUp(int keycode) {
		if(dialogueBox.isVisible() && !dialogueBox.isFinished()) {
			return false;
		}
		if(traverser != null && keycode == Keys.A) {
			if(traverser.getNode().getPointers().isEmpty()) {
				traverser = null;
				dialogueBox.setVisible(false);
				finished = true;
			}
			else {
				progress(0);
			}
			return true;
		}
		if(dialogueBox.isVisible()) {
			return true;
		}
		return false;
	}
	
	public void startDialogue(Dialogue dialogue) {
		traverser = new DialogueTraverser(dialogue);
		dialogueBox.setVisible(true);
		DialogueNode next = traverser.getNode();
		
		DialogueNode node = next;
		dialogueBox.animateText(node.getText());
		
	}
	
	private void progress(int id) {
		DialogueNode next = traverser.getNext(id);
		
		DialogueNode node = next;
		dialogueBox.animateText(node.getText());
	}
	

	
	public boolean getFinished() {
		return finished;
	}
	
	
	
}
