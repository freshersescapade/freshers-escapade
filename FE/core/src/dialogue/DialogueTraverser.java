package dialogue;

public class DialogueTraverser {
	
	private Dialogue dialogue;
	private DialogueNode currentNode;
	
	public DialogueTraverser(Dialogue dialogue) {
		this.dialogue = dialogue;
		currentNode = dialogue.getNode(0);
	}
	
	public DialogueNode getNext(int index) {
		if(currentNode.getPointers().isEmpty()) {
			return null;
		}
		DialogueNode next = dialogue.getNode(currentNode.getPointers().get(index));
		currentNode = next;
		return next;
	}
	
	public DialogueNode getNode() {
		return currentNode;
	}
	
}
