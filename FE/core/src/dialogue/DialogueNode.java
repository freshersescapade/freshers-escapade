package dialogue;

import java.util.ArrayList;

public class DialogueNode {
	
	private String text;
	private int id;
	private ArrayList<Integer> pointers = new ArrayList<Integer>();
	
	public DialogueNode(String text, int id) {
		this.text = text;
		this.id = id;
	}
	
	public void setNext(int id) {
		pointers.add(id);
	}
	
	public String getText() {
		return text;
	}
	
	public int getID() {
		return id;
	}
	
	public ArrayList<Integer> getPointers() {
		return pointers;
	}
	
}
