package dialogue;

import java.util.ArrayList;

public class Dialogue {
	
	private ArrayList<DialogueNode> nodes = new ArrayList<DialogueNode>();
	
	public DialogueNode getNode(int id) {
		return nodes.get(id);
	}
	
	public void addNode(DialogueNode node) {
		nodes.add(node);
	}
	
	public int size() {
		return nodes.size();
	}
	
}
