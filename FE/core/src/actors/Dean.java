package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Dean extends BaseActor{
	
	public Dean(float x, float y, Stage s) {
		super(x, y, s);
		loadTexture("gameScreen/dean.png");
		setScale(0.6f);
		setBoundaryPolygon(6);
	}
}
