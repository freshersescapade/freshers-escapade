package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Fireball extends BaseActor {
	public Fireball(float x, float y, Stage s) {
		super(x, y, s);
		loadAnimationFromSheet("battleScreen/fireball.png", 1, 8, 0.1f, false);
	}

	public void act (float dt) {
		super.act(dt);
		
		if ( isAnimationFinished() ) {
			remove();
		}
	}

}
