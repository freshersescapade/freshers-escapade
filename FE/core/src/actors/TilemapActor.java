package actors;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class TilemapActor extends Actor{
	
	/* Variables for the dimensions of the window. */
	public static int windowWidth = 960;
	public static int windowHeight = 540;

	private TiledMap tiledMap;
	private OrthographicCamera tiledCamera;
	private OrthogonalTiledMapRenderer tiledMapRenderer;

	public TilemapActor(String filename,Stage theStage) {
		tiledMap = new TmxMapLoader().load(filename);
		
		/* Map properties. */
		int tileWidth	= (int)tiledMap.getProperties().get("tilewidth");
		int tileHeight	= (int)tiledMap.getProperties().get("tileheight");
		int numTilesHorizontal = (int)tiledMap.getProperties().get("width");
		int numTilesVertical = (int)tiledMap.getProperties().get("height");
		int mapWidth = tileWidth * numTilesHorizontal;
		int mapHeight = tileHeight * numTilesVertical;
		
		/* Initialise renderer and camera for map. */
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
		
		tiledCamera = new OrthographicCamera();
		tiledCamera.setToOrtho(false,windowWidth,windowHeight);
		tiledCamera.update();
		
		theStage.addActor(this);  // Add the object to the stage so it can be drawn.
		
		BaseActor.setWorldBounds(mapWidth, mapHeight);	// Sets world bounds to the map height and width

	}

	public void act(float dt) {
		super.act(dt);
	}
	
	public void draw(Batch batch, float parentAlpha) {
	 // Keeps camera in position with the main camera 
	 Camera mainCamera = getStage().getCamera();
	 tiledCamera.position.x = mainCamera.position.x;
	 tiledCamera.position.y = mainCamera.position.y;
	 tiledCamera.update();
	 tiledMapRenderer.setView(tiledCamera);
	 
	 /* Batches and renders the map first. */
	 batch.end();
	 tiledMapRenderer.render();
	 batch.begin();
	 }
	
	/*
	 * Iterates through map layers for rectangular map objects that contain a property called "name" and a String value of propertyName
	 * Add those objects to an array list.
	 */
	public ArrayList<MapObject> getRectangleList(String propertyName) {
	 ArrayList<MapObject> list = new ArrayList<MapObject>();
	 
	 for ( MapLayer layer : tiledMap.getLayers() ) {
		 for ( MapObject obj : layer.getObjects() ) {
			 	if ( !(obj instanceof RectangleMapObject) )
			 			continue;
			 	
			 	MapProperties props = obj.getProperties();
	
			if ( props.containsKey("name") && props.get("name").equals(propertyName) )
				list.add(obj);
		 }
	 }
	 return list;
	}
	
	/*
	 * Iterates through layers for objects with a tiled map tile that contain a property called "name" and a String value of propertyName
	 * If the object property does not have a value, assign it a default value.
	 */
	public ArrayList<MapObject> getTileList(String propertyName)
    {
        ArrayList<MapObject> list = new ArrayList<MapObject>();

        for ( MapLayer layer : tiledMap.getLayers() ) {
            for ( MapObject obj : layer.getObjects() )
            {
                if ( !(obj instanceof TiledMapTileMapObject) )
                    continue;

                MapProperties props = obj.getProperties();

                /* Default MapProperties are stored in the tile-set properties while instance specific properties are stored in the map object and override the default.*/
                TiledMapTileMapObject tmtmo = (TiledMapTileMapObject)obj;
                TiledMapTile t = tmtmo.getTile();
                MapProperties defaultProps = t.getProperties();

                if ( defaultProps.containsKey("name") && defaultProps.get("name").equals(propertyName) )
                    list.add(obj);
                
                Iterator<String> propertyKeys = defaultProps.getKeys();  // Get list of default property keys.
                
                while ( propertyKeys.hasNext() ) // Loop through keys, assign default values if none are assigned.
                {
                    String key = propertyKeys.next();
    
                    if ( props.containsKey(key) )
                        continue;
                    else
                    {
                        Object value = defaultProps.get(key);
                        props.put( key, value );
                    }
                }
            }
        }
        return list;
    }
}
