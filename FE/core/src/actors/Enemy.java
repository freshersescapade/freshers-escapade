package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Enemy extends BaseActor{
	
	private int enemy1Hp;
	private int enemy2Hp;
	private int enemy3Hp;
	private int minDmg;
	private int maxDmg;
	private int ID;

	public Enemy(float x, float y, Stage s) {
		super(x, y, s);
	}
	
	public void setID(int id) {
		ID = id;
		
		switch(ID) {
			case(0) : {
				loadTexture("gameScreen/enemy.png");
				setSize(42, 61);
			} break;
			case(1) : {
				loadTexture("gameScreen/monster.png");
				setSize(33, 31);
			} break;
			case(2) : {
				loadTexture("gameScreen/3monsters.png");
				setSize(96, 31);
			} break;
			case(3) : {
				loadTexture("gameScreen/boss1.png");
				setSize(150, 80);
			} break;
			case(4) : {
				loadTexture("gameScreen/Skeleton.png");
				setSize(65, 62);
			} break;
			case(5) : {
				loadTexture("gameScreen/Skeleton.png");
				setSize(100, 124);
			} break;
			case(6) : {
				loadTexture("gameScreen/daemon.png");
				setSize(150, 150);
			} break;
			case(7) : {
				loadTexture("gameScreen/greeneyes.png");
				setSize(100, 92);
			} break;
			case(8) : {
				loadTexture("gameScreen/redeyes.png");
				setSize(100,109);
			} break;
			case(9) : {
				loadTexture("gameScreen/enemy.png");
				setSize(52, 81);
			}
		}
		setBoundaryPolygon(6);
	}

	public int getID() {
		return ID;
	}
	
	// sets enemy health points
	public void setE1Hp(int health) {
		enemy1Hp = health;
	}
	
	public void setE2Hp(int health) {
		enemy2Hp = health;
	}
	
	public void setE3Hp(int health) {
		enemy3Hp = health;
	}
	
	public int getE1Hp() {
		return enemy1Hp;
	}
	
	public int getE2Hp() {
		return enemy2Hp;
	}
	
	public int getE3Hp() {
		return enemy3Hp;
	}
	
	public void setMinDmg(int dmg) {
		minDmg = dmg;
	}
	
	public int getMinDmg() {
		return minDmg;
	}
	
	public void setMaxDmg(int dmg) {
		maxDmg = dmg;
	}
	
	public int getMaxDmg() {
		return maxDmg;
	}
}
	

	
	


