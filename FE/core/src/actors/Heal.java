package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Heal extends BaseActor{

	public Heal(float x, float y, Stage s) {
		super(x, y, s);
		
		loadAnimationFromSheet("battleScreen/healingAnimation.png", 14, 1, 0.1f, false);
	}
	
	public void act (float dt) {
		super.act(dt);
		
		if ( isAnimationFinished() ) {
			remove();
		}
	}
}
 