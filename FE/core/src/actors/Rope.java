package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Rope extends BaseActor{
	
	private String ID;

	public Rope(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		
		setSize(width, height);
	}
	
	public void setID(String id) {
		ID = id;
		
		if(ID.equals("Rope1")) {
			loadTexture("gameScreen/rope1.png");
		}
		else if (ID.equals("Rope2")) {
			loadTexture("gameScreen/rope2.png");
		}
		else if (ID.equals("Rope3")) {
			loadTexture("gameScreen/rope3.png");
		}
	}

	public String getID() {
		return ID;
	}
}
