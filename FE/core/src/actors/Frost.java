package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Frost extends BaseActor{
	public Frost(float x, float y, Stage s) {
		super(x, y, s);
		
		loadAnimationFromSheet("battleScreen/frost.png", 20, 1, 0.1f, false);
	}
	
	public void act (float dt) {
		super.act(dt);
		
		if ( isAnimationFinished() ) {
			remove();
		}
	}
}
