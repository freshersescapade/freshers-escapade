package actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

public class HealthBar extends BaseActor{

	private NinePatchDrawable loadingBarBackground;
    private NinePatchDrawable loadingBar;

    public HealthBar(float x, float y, Stage s) {
    	super(x, y, s);
    	
    	NinePatch loadingBarBackgroundPatch = new NinePatch(new Texture(Gdx.files.internal("battleScreen/blood_red_bar.png")), 20, 20, 10, 10);
        NinePatch loadingBarPatch = new NinePatch(new Texture(Gdx.files.internal("battleScreen/blood_red_bar.png")), 20, 20, 10, 10);
        loadingBar = new NinePatchDrawable(loadingBarPatch);
        loadingBarBackground = new NinePatchDrawable(loadingBarBackgroundPatch);
              
    }

    
    public void draw(Batch batch, double parentAlpha) {
        float progress = 0.4f;

        batch.begin();
        loadingBarBackground.draw(batch, getX(), getY(), getWidth() * getScaleX(), getHeight() * getScaleY());
        loadingBar.draw(batch, getX(), getY(), progress * getWidth() * getScaleX(), getHeight() * getScaleY());
        batch.end();
        
    }
}
	

