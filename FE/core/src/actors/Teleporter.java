package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Teleporter extends BaseActor{
	
	private String ID;

	public Teleporter(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		
		setSize(width, height);
		setBoundaryPolygon(6);
	}
	
	public void setID(String id) {
		ID = id;
	}

	public String getID() {
		return ID;
	}

}
