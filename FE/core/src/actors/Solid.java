package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Solid extends BaseActor{
	
	/*
	 * Class used to denote solid objects/regions.
	 */
	public Solid(float x, float y, float width, float height, Stage s) {
		super(x, y, s);
		
		setSize(width, height);
		setBoundaryRectangle();	// Gives every solid object a default boundary rectangle
	}
}
