package actors;

import com.badlogic.gdx.scenes.scene2d.Stage;

import dialogue.Dialogue;

public class StationaryCharacter extends BaseActor{

	private Dialogue dialogue;
	private PlayableCharacter pc;
	
	public StationaryCharacter(float x, float y, Stage s, String filePath, Dialogue dialogue, PlayableCharacter pc) {
		super(x, y, s);
		loadTexture(filePath);
		setScale(0.5f);
		setBoundaryPolygon(6);
		this.dialogue = dialogue;
		this.pc = pc;
	}
	
	public Dialogue getDialogue() {
		return dialogue;
	}
	
	public PlayableCharacter getPC() {
		return this.pc;
	}

}
