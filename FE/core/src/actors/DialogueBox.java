package actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import game.BaseGame;

public class DialogueBox extends BaseActor{
	
	private Label dialogueLabel;
	private float padding = 16;
	
	private float aniTimer = 0f;
	private float aniTotalTime = 0f;
	private static final float TIME_PER_CHARACTER = 0.05f;
	
	private enum state {RUNNING, IDLE};
	private state dBoxState = state.IDLE;
	String text = "";

	public DialogueBox(float x, float y, Stage s) {
		super(x, y, s);
		loadTexture("dialogBox.png");
		
		dialogueLabel = new Label("",BaseGame.labelStyle);
		dialogueLabel.setWrap(true);
		dialogueLabel.setAlignment(Align.topLeft);
		dialogueLabel.setPosition(padding, padding);
		this.setDialogueSize( getWidth(), getHeight() );
		this.addActor(dialogueLabel);
	}
	
	public void animateText(String text) {
		this.text = text;
//		aniTotalTime = text.length()*TIME_PER_CHARACTER;
		aniTotalTime = TIME_PER_CHARACTER;
		dBoxState = state.RUNNING;
		aniTimer = 0f;
	}
	
//	public void finishAnimateText() {
//		dBoxState = state.COMPLETE;
//	}
//	
	@Override
	public void act(float dt) {
//		if(dBoxState == state.RUNNING) {
//			aniTimer += dt;
//			if(aniTimer > aniTotalTime) {
//				dBoxState = state.IDLE;
//				aniTimer = aniTotalTime;
//			}
//			String displayText = "";
//			int charsToDisplay = (int)((aniTimer/aniTotalTime)*text.length());
//			for(int i=0;i<charsToDisplay;i++) {
//				displayText += text.charAt(i);
//			}
//			if(!displayText.equals(dialogueLabel.getText().toString())) {
//				setText(displayText);
//			}
//		}
//		if(dBoxState == state.COMPLETE) {
//			setText(text);
//		}
//		
		if(dBoxState == state.RUNNING) {
			aniTimer += dt;
			if(aniTimer > aniTotalTime) {
				dBoxState = state.IDLE;
				aniTimer = aniTotalTime;
			}
			setText(text);
		}
		
	}
	
	public boolean isFinished() {
		if (dBoxState == state.IDLE)
			return true;
		else
			return false;
	}
	
	public void setDialogueSize(float width, float height) {
		this.setSize(width, height);
		dialogueLabel.setWidth( width - 2 * padding );
		dialogueLabel.setHeight( height - 2 * padding );
	}
	
	public void setText(String text) { 
		dialogueLabel.setText(text); 
	}
	
	 public void setFontScale(float scale) { 
		 dialogueLabel.setFontScale(scale); 
	 }
	 
	 public void setFontColor(Color color) {
		 dialogueLabel.setColor(color);
	 }
	 
	 public void setBackgroundColor(Color color) {
		 this.setColor(color);
	 }
	 
	 public void alignTopLeft() {
		 dialogueLabel.setAlignment( Align.topLeft ); 
	 }
	 
	 public void alignCenter() {
		 dialogueLabel.setAlignment( Align.center ); 
	 }
	 
	 public void alignBottom() {
		 dialogueLabel.setAlignment( Align.bottom);
	 }
}
