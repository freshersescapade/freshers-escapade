package actors;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Intersector.MinimumTranslationVector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;


public class BaseActor extends Group {
	
	// Animation variables
	private Animation<TextureRegion> animation;
	private float elapsedTime;
	private boolean animationPaused;
	
	// Movement variables
	private Vector2 velocityVec;
	private Vector2 accelerationVec;
	private float acceleration;
	private float maxSpeed;
	private float deceleration;
	
	// Boundary polygon for collision
	private Polygon boundaryPolygon;
	
	// Boundary for world
	private static Rectangle worldBounds;
	
	
	public BaseActor(float x, float y, Stage s) {
		super();
		
		setPosition(x,y);
		s.addActor(this);
		
		animation = null;
		elapsedTime = 0;
		animationPaused = false;
		
		velocityVec = new Vector2(0,0);
		accelerationVec = new Vector2(0,0);
		acceleration = 0;
		maxSpeed = 1000;
		deceleration = 0;
		
		boundaryPolygon = null;
	}

	/*
	 * Used for setting the width and height of the Actor and the origin from which the animation can be transformed.
	 * The width and height are, in this case, set to the width and height of the first image in the sprite sheet.
	 */
	public void setAnimation(Animation<TextureRegion> anim) {
		animation = anim;
		TextureRegion tr = animation.getKeyFrame(0);
		float w = tr.getRegionWidth();
		float h = tr.getRegionHeight();
		setSize( w, h );
		setOrigin( w/2, h/2 );
		
		if (boundaryPolygon == null)
			setBoundaryRectangle();
	}
	
	/*
	 * Used for changing state of the animation (either paused or playing).		
	 */
	public void setAnimationPaused(boolean pause) {
		animationPaused = pause;
	}
	
	/*
	 * Determines if the animation is finished by checking if the elapsed time corresponds with the expected elapsed time for the animation.
	 */
	public boolean isAnimationFinished() {
	 return animation.isAnimationFinished(elapsedTime);
	}

	/*
	 * (non-Javadoc)
	 * @see com.badlogic.gdx.scenes.scene2d.Group#act(float)
	 * Called each frame by the stage, this function is responsible for keeping track of an automatically updating the elapsed time for the animation.
	 */
	public void act(float dt) {
		super.act( dt );
		if (!animationPaused) {
			elapsedTime += dt;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.badlogic.gdx.scenes.scene2d.Group#draw(com.badlogic.gdx.graphics.g2d.Batch, float)
	 * If the animation is set (not null) and visible, draw the animation frame corresponding to the elapsed time in the correct position.
	 */
	public void draw(Batch batch, float parentAlpha) {
		Color c = getColor();
		batch.setColor(c.r, c.g, c.b, c.a);
		
		if ( animation != null && isVisible() ) {
			batch.draw( animation.getKeyFrame(elapsedTime),
					getX(), getY(), getOriginX(), getOriginY(),
					getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation() );
			super.draw(batch, parentAlpha);
		}
	}
	
	/*
	 * Creates an animation loop from separate files of images.
	 */
	public Animation<TextureRegion> loadAnimationFromFiles(String[] fileNames, float frameDuration, boolean loop){ 
        int fileCount = fileNames.length;
        Array<TextureRegion> textureArray = new Array<TextureRegion>();

        for (int n = 0; n < fileCount; n++){   
            String fileName = fileNames[n];
            Texture texture = new Texture( Gdx.files.internal(fileName) );
            texture.setFilter( TextureFilter.Linear, TextureFilter.Linear );
            textureArray.add( new TextureRegion( texture ) );
        }
            
        Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, textureArray);

        if (loop) {
            anim.setPlayMode(Animation.PlayMode.LOOP);
        }else {
            anim.setPlayMode(Animation.PlayMode.NORMAL);
        }

        if (animation == null) {
            setAnimation(anim);
        }

        return anim;
	}

	/*
	 * Creates the animation loop based on a sprite sheet.
	 */
	public Animation<TextureRegion> loadAnimationFromSheet(String fileName, int rows, int cols, float frameDuration, boolean loop) 
	{
		 Texture texture = new Texture(Gdx.files.internal(fileName), true);
		 texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		 int frameWidth = texture.getWidth() / cols;
		 int frameHeight = texture.getHeight() / rows;
		 
		 TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight); 
		 
		 Array<TextureRegion> textureArray = new Array<TextureRegion>();
		 
		 /* Places sub-images from sprite sheet into the same position in a texture array as they were in the sprite sheet. */
		 for (int r = 0; r < rows; r++) 
			 for (int c = 0; c < cols; c++)
				 textureArray.add(temp[r][c]);
		 
		 Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration,textureArray);
		 
		 if (loop)
			 anim.setPlayMode(Animation.PlayMode.LOOP);
		 else
			 anim.setPlayMode(Animation.PlayMode.NORMAL);
		 
		 if (animation == null)
			 setAnimation(anim);
		 
		 return anim; 
	}
	
	/* Creates a one frame animation for loading an image onto the screen. */
	public Animation<TextureRegion> loadTexture(String fileName) {
        String[] fileNames = new String[1];
        fileNames[0] = fileName;
        return loadAnimationFromFiles(fileNames, 1, true);
    }     

	/*
	 * Sets the speed of movement in the direction currently travelling. If the speed is zero, the current direction is set to north (0 degrees).
	 * The following methods are used for player movement
	 */
	public void setSpeed(float speed) {
        if (velocityVec.len() == 0)
            velocityVec.set(speed, 0);
        else
        	velocityVec.setLength(speed);
	}
	
	public float getSpeed() {
        return velocityVec.len();
	}
        	
	public float getMotionAngle() {
        return velocityVec.angle();
	}
	    
	public boolean isMoving() {
		return (getSpeed() > 0);
	}
	
	public void setAcceleration(float acc) {
		acceleration = acc;
	}
	
	public void accelerateAtAngle(float angle) {
		accelerationVec.add(new Vector2(acceleration, 0).setAngle(angle));
	}
	
	public void accelerateForward() {
		accelerateAtAngle(getRotation());
	}
	
	public void setMaxSpeed(float ms) {
		maxSpeed=ms;
	}
	
	public void setDeceleration(float dec) {
		deceleration = dec;
	}

	/*
	 * Function for updating velocity vector according to the acceleration vector.	
	 */
	public void applyPhysics(float dt) {
        velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );

        float speed = getSpeed();

        if (accelerationVec.len() == 0) // If the acceleration is zero, apply deceleration.
            speed -= deceleration * dt;

        speed = MathUtils.clamp(speed, 0, maxSpeed); // Ensures speed does not exceed the maximum speed.

        setSpeed(speed);

        moveBy(velocityVec.x * dt, velocityVec.y * dt);
    
        accelerationVec.set(0,0);
    }

	/*
	 * Creates rectangular polygon for collision, called when animation is set if there is no boundary polygon.
	 */
	public void setBoundaryRectangle() {
		float w = getWidth();
		float h = getHeight();
		
		float[] vertices = {0,0, w,0, w,h, 0,h};
		boundaryPolygon = new Polygon(vertices);
	}

	/*
	 * Creates a collision polygon with n sides. 
	 * Useful because a polygon object can be rotated and so provides better detection for entities that can rotate i.e. player.
	 * Also prevents collision on corners with transparent pixels.
	 */
	public void setBoundaryPolygon(int numSides) {
	        float w = getWidth();
	        float h = getHeight();

	        float[] vertices = new float[2*numSides];
	        for (int i = 0; i < numSides; i++)
	        {
	        	/* The number 6.28 is used because the functions cosine and sine are periodic between 0 and 2 pi. 
	        	 * width/2 and height/2 are added to centre the side generation around the centre of actor rather than the origin. */
	            float angle = i * 6.28f / numSides;	         
	            vertices[2*i] = w/2 * MathUtils.cos(angle) + w/2; // x coordinate for generation
	            vertices[2*i+1] = h/2 * MathUtils.sin(angle) + h/2; // y coordinate for generation
	        }
	        boundaryPolygon = new Polygon(vertices);
	 }

	public Polygon getBoundaryPolygon() {
        boundaryPolygon.setPosition( getX(), getY() );
        boundaryPolygon.setOrigin( getOriginX(), getOriginY() );
        boundaryPolygon.setRotation( getRotation() );
        boundaryPolygon.setScale( getScaleX(), getScaleY() );        
        return boundaryPolygon;
    }
	
		/* This method checks if one actors overlaps another actor */
	public boolean overlaps(BaseActor other) {
        Polygon poly1 = this.getBoundaryPolygon();
        Polygon poly2 = other.getBoundaryPolygon();

        /* This if statement first checks if the rectangles of the objects overlap. 
         * This is much simpler and less intensive calculation than checking if the polygons of the shapes overlap all the time.
         * If the rectangles of the objects do not overlap than the polygons won't overlap either. */
        if ( !poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle()) )
            return false;

        return Intersector.overlapConvexPolygons( poly1, poly2 );
    }

	/* Centres actor at a given position, instead of the default bottom left of a location.*/
	public void centerAtPosition(float x, float y) {

	        setPosition( x - getWidth()/2 , y - getHeight()/2 );
	    }
	
	/* Centres actor at centre of another actor */
	public void centerAtActor(BaseActor other) {
		centerAtPosition( other.getX() + other.getWidth()/2, other.getY() + other.getHeight()/2);
	}
	
	public void setOpacity(float opacity) {
	        this.getColor().a = opacity; // Enables changing the opacity of the actor.
	    }
	 
	/*
	 * Prevents overlapping by translating the actor by the minimum vector required for the actors' polygons to not overlap.
	 */
	public Vector2 preventOverlap(BaseActor other) {
        Polygon poly1 = this.getBoundaryPolygon();
        Polygon poly2 = other.getBoundaryPolygon();

        if ( !poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle()) ) {
            return null;
        }

        MinimumTranslationVector mtv = new MinimumTranslationVector();
        boolean polygonOverlap = Intersector.overlapConvexPolygons(poly1, poly2, mtv);

        if ( !polygonOverlap )
            return null;

        this.moveBy( mtv.normal.x * mtv.depth, mtv.normal.y * mtv.depth );
        return mtv.normal;
    }
	
	/*
	 * This function establishes an ArrayList of actors in a given stage of a specific class to facilitate easier management of them.
	 * A try-catch block is used to ensure that the given class name is valid and isInstance checks if the object is an instance of the class or of a class that extends the given class. 
	 */
	public static ArrayList<BaseActor> getList(Stage stage, String className) {
	        ArrayList<BaseActor> list = new ArrayList<BaseActor>();
	        
	        Class<?> theClass = null;
	        try
	        {  theClass = Class.forName(className);  }
	        catch (Exception error)
	        {  error.printStackTrace();  }
	        
	        for (Actor a : stage.getActors())
	        {
	            if ( theClass.isInstance( a ) )
	                list.add( (BaseActor)a );
	        }

	        return list;
	    }
	
	/* Counts the number of specific objects on the stage */
	 
	public static int count(Stage stage, String className) {
	  return getList(stage, className).size();
	 }
	
	/* Sets world bounds as a rectangle. */
	public static void setWorldBounds(float width, float height) {
		worldBounds = new Rectangle(0,0,width,height);
	}
	
	/* Sets world bounds by width and height of a baseActor. */
	public static void setWorldBounds(BaseActor ba) {
		setWorldBounds( ba.getWidth(), ba.getHeight() );
	}
	
	public static Rectangle getWorldBounds() {
		return worldBounds;
	}
	
	/* If an object begins to move past the world boundaries its position is changed to ensure it stays on the screen entirely. */
	public void boundToWorld() {
        if (getX() < 0) {
            setX(0);
        }
        if (getX() + getWidth() > worldBounds.width) {   
            setX(worldBounds.width - getWidth());
        }
        if (getY() < 0) {
            setY(0);
        }
        if (getY() + getHeight() > worldBounds.height) {
            setY(worldBounds.height - getHeight());
        }
    }
	
	/*
	 * Function which makes it such that player is only able to view a part of the world at a time.
	 * Centres the camera on the player and sets the camera to only view an area the size of the window.
	 */
	public void alignCamera()
    {
        Camera cam = this.getStage().getCamera();
        //Viewport v = this.getStage().getViewport();

        cam.position.set( this.getX() + this.getOriginX(), this.getY() + this.getOriginY(), 0 ); // Centres camera 

        /* Ensures that the camera does not go beyond the rendered game world by keeping the camera within half a view distance
         * away from the rendered game world. */
        cam.position.x = MathUtils.clamp(cam.position.x, cam.viewportWidth/2,  worldBounds.width -  cam.viewportWidth/2);
        cam.position.y = MathUtils.clamp(cam.position.y, cam.viewportHeight/2, worldBounds.height - cam.viewportHeight/2);
        cam.update();
    }
	 
	 
}
