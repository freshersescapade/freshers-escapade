package actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;

public class PlayableCharacter extends BaseActor implements Serializable{
	
	
	// Animation variables 
	Animation<TextureRegion> north;
	Animation<TextureRegion> south;
	Animation<TextureRegion> east;
	Animation<TextureRegion> west;
	float facingAngle;
	
	private int heal, mana, damage, hp;
	private boolean blocking = false, alive = true;
	private String name;

	public PlayableCharacter(float x, float y, Stage s, int hp, int damage, int mana, int heal, String filePath, String name) {
		super(x,y,s);
		
		this.hp = hp;
		this.damage = damage;
		this.mana = mana;
		this.heal = heal;
		this.name = name;
		
		String fileName = filePath; 
		int rows = 4; // Number of rows in sprite sheet.
		int cols = 4; // Number of columns in sprite sheet.
		Texture texture = new Texture(Gdx.files.internal(fileName), true);
		int frameWidth = texture.getWidth() /cols; 
		int frameHeight = texture.getHeight() /rows;
		float frameDuration = 0.2f; // Time spent on each frame of animation.
		
		TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight);
		
		/* Separates sprite sheet into separate images for the four cardinal directions and creates an animation for each direction. 
		 * The first 4 images in the first column are used for facing South, the next column for West, then East and finally North
		 * The animations loops through the 4 images to show player movement */
		
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		for (int c = 0; c < cols; c++) 
			textureArray.add( temp[0][c] );
			south = new Animation<TextureRegion>(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
			
			textureArray.clear();
		for (int c = 0; c < cols; c++) 
			textureArray.add( temp[1][c] );
			west = new Animation<TextureRegion>(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
				
			textureArray.clear();
		for (int c = 0; c < cols; c++) 
			textureArray.add( temp[2][c] );
			east = new Animation<TextureRegion>(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
					
			textureArray.clear();
		for (int c = 0; c < cols; c++) 
			textureArray.add( temp[3][c] );
			north = new Animation<TextureRegion>(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		setAnimation(south); // Sets default facing direction and animation to south
		facingAngle = 270;
		
		setBoundaryPolygon(8); // Creates a collision polygon of eight sides for the player.
		setAcceleration(400);  // Sets player movement speed
		setMaxSpeed(100);
		setDeceleration(400);	
		
	}
	
	
	public void act(float dt) {
		super.act(dt);
		
		// Pauses the animation if the player is stationary.
		if (getSpeed() == 0) 
			setAnimationPaused(true);
		else {
				setAnimationPaused(false);
				
				float angle = getMotionAngle();
				/* Calculates the angle the player is facing and plays the corresponding animation.
				 * The animations loops through the 4 images in each of the 4 columns to show movement animation 
				 * in the direction the player is currently facing */
				if(angle >= 45 && angle <= 135) {
					facingAngle = 90;
					setAnimation(north);	
				}
				else if(angle > 135 && angle < 225) {
					facingAngle = 180;
					setAnimation(west);
				}
				else if(angle >= 225 && angle <= 315) {
					facingAngle = 270;
					setAnimation(south);
				}
				else {
					facingAngle = 0;
					setAnimation(east);
				}
		}
		alignCamera();	// Centres camera on player
		boundToWorld();	// Bounds player to world (player cannot walk out of map bounds)
		applyPhysics(dt);	// Updates player velocity and acceleration	for movement
	}
	
	public float getFacingAngle() {
		return facingAngle;
	}
	
	public int getHp() {
		return hp;
	}
	
	public int getDamage() {
		return damage;
	}
	
	public int getMana() {
		return mana;
	}
	
	public int getHeal() {
		return heal;
	}
	
	public boolean getAlive() {
		return alive;
	}
	
	public boolean getBlocking() {
		return blocking;
	}
	
	public String getName() {
		return name;
	}
	
	public void setHp(int hp) {
		this.hp = hp;
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	public void setMana(int mana) {
		this.mana = mana;
	}
	
	public void setHeal(int heal) {
		this.heal = heal;
	}
	
	public void setAlive(boolean bool) {
		alive = bool;
	}
	
	public void setBlocking(boolean bool) {
		blocking = bool;
	}

	@Override
	public void write(Json json) {
		// TODO Auto-generated method stub	
	}


	@Override
	public void read(Json json, JsonValue jsonData) {
		// TODO Auto-generated method stub
		
	}

}
