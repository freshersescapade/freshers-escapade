package com.mygame.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import game.FreshersEscapade;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Game myGame = new FreshersEscapade();
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.addIcon("logo.png", FileType.Internal);
		config.width = 1920;
		config.height = 1080;
		config.title = "Freshers Escapade";
		new LwjglApplication(myGame, config);
	}
}
