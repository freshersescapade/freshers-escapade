# TeamProject

## Setup
For Windows:
1. Download [Git for Windows](https://gitforwindows.org/)
2. Open Git Bash (as Admin)

For Mac/Linux: Use Terminal

## Cloning into file system

1. Change directory to required location using terminal/git bash
2. Clone the repo into the chosen directory: 

```bash
git clone https://gitlab.com/freshersescapade/freshers-escapade
and project is now cloned into directory
```


## Optional - Sourcetree: A GUI for Git

SourceTree makes it easier to track changes in Git

1. Download [Sourcetree](https://www.sourcetreeapp.com/)
2. Click add -> find the appropriate file